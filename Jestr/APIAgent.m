//
//  APIAgent.m
//  Jestr
//
//  Created by Árpád Kiss on 2014.04.27..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import "APIAgent.h"
#import "CurrentUser.h"

@implementation APIAgent {
    AFHTTPRequestOperationManager *_manager;
    NSString *_version;
    NSString *_apiURL;
    NSString *_cookiePrefix;
}

-(id)init{
    if(self = [super init]) {
        _version = @"v1";
        _apiURL = @"http://api.jestr.me";
        _cookiePrefix = @"jstr.sess";
        _manager = [AFHTTPRequestOperationManager manager];
        [_manager setRequestSerializer:[AFJSONRequestSerializer serializer]];
        [_manager setResponseSerializer:[AFJSONResponseSerializer serializer]];
    }
    return self;
}

-(void)postToEndpoint:(NSString*)endpoint
           withParams:(NSDictionary*)parameters
            onSuccess: (void(^)(id responseObject))successBlock
               onFail: (void(^)(id error))failBlock {
    
    NSLog(@"%@",parameters.description);
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];

    [_manager POST:[NSString stringWithFormat:@"%@/%@/%@", _apiURL, _version, endpoint] parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];

        
        id json = [responseObject objectForKey:@"data"];
        
        successBlock(json);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];

        if(error.code == -1009) { // 1011 400-as error kód
            [self dropNetworkError];
        }
        NSLog(@"%@",operation.description);
        failBlock(operation.responseObject);
    }];
}

-(void)postToEndpoint:(NSString*)endpoint
           withParams:(NSDictionary*)parameters
            withImage:(UIImage *)image
              andName:(NSString *)name
            onSuccess: (void(^)(id responseObject))successBlock
               onFail: (void(^)(NSError *error))failBlock {
    
    NSLog(@"%@",parameters.description);
    
    NSData *imageData = UIImageJPEGRepresentation(image, 1);
    
    NSString *fileName = [NSString stringWithFormat:@"%ld%c%c.jpg", (long)[[NSDate date] timeIntervalSince1970], arc4random_uniform(26) + 'a', arc4random_uniform(26) + 'a'];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];

    [_manager POST:[NSString stringWithFormat:@"%@/%@/%@", _apiURL, _version, endpoint]
        parameters:parameters
    constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileData:imageData name:name fileName:fileName mimeType:@"image/jpeg"];
    }
           success:^(AFHTTPRequestOperation *operation, id responseObject) {
               [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];

               id json = [responseObject objectForKey:@"data"];
               
               successBlock(json);
               
           } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
               [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];

               if(error.code == -1009) { // 1011 400-as error kód
                   [self dropNetworkError];
               }
               NSLog(@"%@",operation.description);
               failBlock(operation.responseObject);
           }];
}

-(void)postToEndpoint:(NSString*)endpoint
           withParams:(NSDictionary*)parameters
            withImages:(NSArray*)images
            onSuccess: (void(^)(id responseObject))successBlock
               onFail: (void(^)(NSError *error))failBlock {

    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];

    [_manager POST:[NSString stringWithFormat:@"%@/%@/%@", _apiURL, _version, endpoint]
        parameters:parameters
constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
    
    [images enumerateObjectsUsingBlock:^(NSDictionary* obj, NSUInteger idx, BOOL *stop){
        NSData *imageData = UIImageJPEGRepresentation([obj objectForKey:@"image"], 1);
        
        NSString *fileName = [NSString stringWithFormat:@"%ld%c%c.jpg", (long)[[NSDate date] timeIntervalSince1970], arc4random_uniform(26) + 'a', arc4random_uniform(26) + 'a'];

        [formData appendPartWithFileData:imageData name:[obj valueForKey:@"name" ] fileName:fileName mimeType:@"image/jpeg"];
        
    }];
}
           success:^(AFHTTPRequestOperation *operation, id responseObject) {
               [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];

               id json = [responseObject objectForKey:@"data"];
               
               successBlock(json);
               
           } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
               [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];

               if(error.code == -1009) { // 1011 400-as error kód
                   [self dropNetworkError];
               }
               NSLog(@"%@",operation.description);
               failBlock(operation.responseObject);
           }];

}


-(void)getFromEndpoint:(NSString*)endpoint
             onSuccess:(void(^)(id responseObject))successBlock
                onFail:(void(^)(id error))failBlock {
    
    NSLog(@"GET......");
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    [_manager GET:[NSString stringWithFormat:@"%@/%@/%@", _apiURL, _version, endpoint] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];

        id json = [responseObject objectForKey:@"data"];
        
        successBlock(json);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];

        if(error.code == -1009) { // 1011 400-as error kód
            [self dropNetworkError];
        }
        NSLog(@"%@",operation.description);
        failBlock(operation.responseObject);
    }];
    
}

-(void)putOnEndpoint:(NSString*)endpoint
          withParams:(NSDictionary*)parameters
           onSuccess:(void(^)(id responseObject))successBlock
              onFail:(void(^)(id error))failBlock{
    NSLog(@"%@",parameters.description);
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    
    [_manager PUT:[NSString stringWithFormat:@"%@/%@/%@", _apiURL, _version, endpoint] parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        
        
        id json = [responseObject objectForKey:@"data"];
        
        successBlock(json);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        
        if(error.code == -1009) { // 1011 400-as error kód
            [self dropNetworkError];
        }
        NSLog(@"%@",operation.description);
        failBlock(operation.responseObject);
    }];

}

-(void)deleteFromEndpoint:(NSString*)endpoint
               withParams:(NSDictionary*)parameters
                onSuccess:(void(^)(id responseObject))successBlock
                   onFail:(void(^)(id error))failBlock{
    NSLog(@"%@",parameters.description);
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    
    [_manager DELETE:[NSString stringWithFormat:@"%@/%@/%@", _apiURL, _version, endpoint] parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        
        
        id json = [responseObject objectForKey:@"data"];
        
        successBlock(json);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        
        if(error.code == -1009) { // 1011 400-as error kód
            [self dropNetworkError];
        }
        NSLog(@"%@",operation.description);
        failBlock(operation.responseObject);
    }];

}

#pragma mark CurrentUser methods

-(BOOL)isUserHaveSession {
    NSArray *cookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage]
                        cookiesForURL: [NSURL URLWithString:_apiURL]
                        ];
    for (NSHTTPCookie *cookie in cookies)
    {
        if([cookie.name isEqualToString:_cookiePrefix]) {
            NSDate *expiresDate = [cookie expiresDate];
            NSDate *currentDate = [NSDate date];
            NSComparisonResult result = [currentDate compare:expiresDate];
            NSLog(@"%@", expiresDate);
            NSLog(@"%@", currentDate);
            NSLog(@"%d", result);
            
            if(result==NSOrderedAscending){
                return YES;
            }
            [[NSHTTPCookieStorage sharedHTTPCookieStorage] deleteCookie:cookie];
            return NO;
        }
    }
    return NO;
}

-(void)registerUserToken:(NSString*)token {
    [self postToEndpoint:@"auth/token" withParams:@{@"token":token} onSuccess:^(id responseObject) {
        
    } onFail:^(id error) {
        NSLog(@"%@",error);
    }];
}

-(void)resetSession {
    NSArray *cookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage]
                        cookiesForURL: [NSURL URLWithString:_apiURL]
                        ];
    for (NSHTTPCookie *cookie in cookies)
    {
        if([cookie.name isEqualToString:_cookiePrefix]) {
            [[NSHTTPCookieStorage sharedHTTPCookieStorage] deleteCookie:cookie];
        }
    }

}

- (void) dropNetworkError {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"It seems, the network is unreachable."
                                                    message:@"Please, try it again."
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

// singleton declaration

+(APIAgent*) sharedAgent {
    static id _sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[self alloc] init];
    });
    return _sharedInstance;
}

@end
