//
//  Thread_Preview.h
//  Jestr
//
//  Created by Árpád Kiss on 2014.07.02..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class User_Public;

@interface Thread_Preview : NSManagedObject

@property (nonatomic, retain) NSArray * posts;
@property (nonatomic, retain) User_Public *friend;

@end
