//
//  Post_Message.m
//  Jestr
//
//  Created by Árpád Kiss on 2014.07.22..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import "Post_Message.h"
#import "User_Public.h"


@implementation Post_Message

@dynamic created_time;
@dynamic id;
@dynamic images;
@dynamic relatedTo;
@dynamic author;

@end
