//
//  ProfileSettingsViewController.m
//  Jestr
//
//  Created by Árpád Kiss on 2014.07.29..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import "ProfileSettingsViewController.h"
#import "Helper.h"
#import "APIAgent.h"

@interface ProfileSettingsViewController () <UITextFieldDelegate>

@end

@implementation ProfileSettingsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.privateSwitch.on = ([self.profile.private isEqualToNumber:@1]);
    self.usernameTextField.text = self.profile.username;
    self.emailTextField.text = [Helper sharedHelper].currentUser.email;
    self.title = self.profile.username;
    
    
    self.usernameTextField.delegate = self;
    self.emailTextField.delegate = self;
    self.passwordTextField.delegate = self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)saveSettings:(id)sender {
    if([self validateFields]) {
        
        NSMutableDictionary* data = [[NSMutableDictionary alloc] init];
        
        [data setObject:self.usernameTextField.text forKey:@"username"];
        [data setObject:self.emailTextField.text forKey:@"email"];
        [data setObject:[[NSNumber alloc] initWithBool: self.privateSwitch.on] forKey:@"private"];
        
        if(self.passwordTextField.text.length > 0) {
            [data setObject:self.passwordTextField.text forKey:@"password"];
        }
        
        [[APIAgent sharedAgent] putOnEndpoint:@"users/me" withParams:data onSuccess:^(NSDictionary* responseObject) {
            NSLog(@"%@", responseObject);
            [Helper sharedHelper].currentUser.email = self.emailTextField.text;
            
            [Helper sharedHelper].currentUser.username = [responseObject valueForKey:@"username"];
            
            self.profile = [User_Profile MR_importFromObject:responseObject inContext:self.inheritedContext];
            
            self.title = @"";
            [self dismissViewControllerAnimated:YES completion:^{
                
            }];
            
        } onFail:^(id error) {
            //
        }];
    }
}

-(BOOL)validateFields {
    BOOL p = [[Helper sharedHelper] validateEmail:self.emailTextField.text] &&
    self.usernameTextField.text.length >= 3 && self.usernameTextField.text.length <= 14;
    return (p && (self.passwordTextField.text.length == 0 || (self.passwordTextField.text.length >= 3 && self.passwordTextField.text.length <= 14)));
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if(textField == self.usernameTextField) {
        [self.usernameTextField resignFirstResponder];
        [self.passwordTextField becomeFirstResponder];
    } else if(textField == self.passwordTextField) {
        [self.passwordTextField resignFirstResponder];
        [self.emailTextField becomeFirstResponder];
    } else if(textField == self.emailTextField) {
        [self.emailTextField resignFirstResponder];
    }
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
