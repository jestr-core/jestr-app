//
//  UserThreadsViewController.m
//  Jestr
//
//  Created by Árpád Kiss on 2014.07.02..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import "UserThreadsViewController.h"
#import "ThreadTableViewCell.h"
#import "APIAgent.h"
#import "Thread_Preview.h"
#import "UserPrivateThreadViewController.h"

@interface UserThreadsViewController () {
    NSArray* _threads;
    NSManagedObjectContext* _tmpContext;
    Thread_Preview* _tmpThread;
}

@end

@implementation UserThreadsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"Messages";
    
    // Do any additional setup after loading the view.
    _tmpContext = [NSManagedObjectContext MR_contextWithParent:[NSManagedObjectContext MR_contextForCurrentThread]];
    UINib* nib = [UINib nibWithNibName:@"ThreadTableViewCell" bundle:nil];
    
    [self.threadsTableView registerNib:nib forCellReuseIdentifier:kThreadCellReuseIdentifier];
    
    [self fetchThreads];
    
}

-(void)fetchThreads {
    NSString* endpoint = @"users/me/messages";
    [[APIAgent sharedAgent] getFromEndpoint:endpoint onSuccess:^(id responseObject) {
        NSLog(@"DATA: %@", responseObject);
        
        if(![responseObject isEqual:[NSNull null]]) {
        
            [responseObject enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                [Thread_Preview MR_importFromObject:obj inContext:_tmpContext];
            }];
        
            _threads = [Thread_Preview MR_findAllInContext:_tmpContext];
        
            [self.threadsTableView reloadData];
        }
        
    } onFail:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _threads.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ThreadTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kThreadCellReuseIdentifier];
    [cell configureCell:_threads[indexPath.row]];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    _tmpThread = _threads[indexPath.row];
    [self performSegueWithIdentifier:@"ToUserThreadFromThreads" sender:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if([segue.identifier isEqualToString:@"ToUserThreadFromThreads"]) {
        UserPrivateThreadViewController* c = (UserPrivateThreadViewController*)segue.destinationViewController;
        c.friend = _tmpThread.friend;
    }
}


@end
