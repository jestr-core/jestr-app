//
//  SearchViewController.h
//  Jestr
//
//  Created by Árpád Kiss on 2014.07.03..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchViewController : UIViewController <
    UITableViewDelegate,
    UITableViewDataSource,
    UISearchBarDelegate
>

@property (weak, nonatomic) IBOutlet UITableView *resultsTableView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@end
