//
//  Notification.m
//  Jestr
//
//  Created by Árpád Kiss on 2014.07.01..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import "Notification.h"
#import "User_Public.h"


@implementation Notification

@dynamic id;
@dynamic type;
@dynamic confirmed;
@dynamic related_object;
@dynamic created_time;
@dynamic from;

@end
