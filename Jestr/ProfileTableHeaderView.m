//
//  ProfileTableHeaderView.m
//  Jestr
//
//  Created by Árpád Kiss on 2014.06.09..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import "ProfileTableHeaderView.h"
#import "UIImageView+AFNetworking.h"
#import "ILTranslucentView.h"
#import "Helper.h"

#define movePOffset @40
#define moveNOffset @-40
#define animDur 0.2

#define MAXLENGTH 260 // text


@implementation ProfileTableHeaderView {
    ILTranslucentView* _aboutView;
    UITextView* _t;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)configureView:(User_Profile*)profile {
    
    //UIInterpolatingMotionEffect *interpolationHorizontal = [[UIInterpolatingMotionEffect alloc]initWithKeyPath:@"center.x" type:UIInterpolatingMotionEffectTypeTiltAlongHorizontalAxis];
    //interpolationHorizontal.minimumRelativeValue = moveNOffset;
    //interpolationHorizontal.maximumRelativeValue = movePOffset;
    
    UIInterpolatingMotionEffect *interpolationVertical = [[UIInterpolatingMotionEffect alloc]initWithKeyPath:@"center.y" type:UIInterpolatingMotionEffectTypeTiltAlongVerticalAxis];
    interpolationVertical.minimumRelativeValue = moveNOffset;
    interpolationVertical.maximumRelativeValue = movePOffset;
    
    //[self.coverImageView addMotionEffect:interpolationHorizontal];
    [self.coverImageView addMotionEffect:interpolationVertical];
    
    NSDictionary* pictures = [profile.profile objectForKey:@"picture"];
    NSDictionary* cover = [profile.profile objectForKey:@"cover"];

    
    NSString *profileImageURL = [@"http://api.jestr.me/"
                                 stringByAppendingString:EMPTYIFNIL(                                                       [pictures valueForKey:@"medium"])];
    
    [self.profileImageView setImageWithURL:[NSURL URLWithString:profileImageURL]];
    
    NSString *coverImageURL = [@"http://api.jestr.me/"
                                 stringByAppendingString:EMPTYIFNIL(                                                       [cover valueForKey:@"xlarge"])];
    
    [self.coverImageView setImageWithURL:[NSURL URLWithString:coverImageURL]];
    
    self.followingLabel.text = [[profile.stats objectForKey:@"following"] stringValue];
    self.followersLabel.text = [[profile.stats objectForKey:@"followers"] stringValue];
    self.imagesLabel.text = [[profile.stats objectForKey:@"posts"] stringValue];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(followersLabelTap:)];
    
    [self.followersContainerView addGestureRecognizer:tapGesture];
    
    tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(followingLabelTap:)];
    [self.followingContainerView addGestureRecognizer:tapGesture];
    
    tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(profilePictureTap:)];
    [self.profileImageView addGestureRecognizer:tapGesture];
    self.profileImageView.userInteractionEnabled = YES;
    
    UILongPressGestureRecognizer* p = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(coverLongTap:)];
    p.minimumPressDuration = 0.3;
    [self.coverImageView addGestureRecognizer:p];
    self.coverImageView.userInteractionEnabled = YES;
    
    self.actionButton.tintColor = [UIColor whiteColor];
    
    NSString* about;
    
    if([profile.profile objectForKey:@"about"]) {
        about = [profile.profile objectForKey:@"about"];
    } else {
        about = @"";
    }
    [self configureAbout:about];

}

-(void)swipe:(UISwipeGestureRecognizer*)sw {
    NSLog(@"%d", sw.state);
    if(sw.direction == UISwipeGestureRecognizerDirectionLeft) {
        [UIView animateWithDuration:animDur animations:^{
            //code
            _aboutView.frame = CGRectMake(0, 0, self.coverImageView.frame.size.width, self.coverImageView.frame.size.height);
        }];
    } else {
        [UIView animateWithDuration:animDur animations:^{
            //code
            _aboutView.frame = CGRectMake(self.coverImageView.frame.size.width, 0, self.coverImageView.frame.size.width, self.coverImageView.frame.size.height);
        }];
    }
}

-(void)configureAbout:(NSString*)about {
    _t = [[UITextView alloc] initWithFrame:CGRectMake(10, 10, self.coverImageView.frame.size.width -20, self.userPrefView.frame.size.height -20)];
    [_t setText:about];
    [_t setFont:[UIFont fontWithName:@"ArialMT" size:16]];
    [_t setTextAlignment:NSTextAlignmentCenter];
    [_t setBackgroundColor:[UIColor clearColor]];
    _aboutView = [[ILTranslucentView alloc] initWithFrame:CGRectMake(self.frame.size.width, 0, self.frame.size.width, self.frame.size.height)];
    
    _aboutView.translucentAlpha = 1;
    _aboutView.translucentStyle = UIBarStyleDefault;
    _aboutView.translucentTintColor = [UIColor clearColor];
    _aboutView.backgroundColor = [UIColor clearColor];
    
    [_aboutView addSubview:_t];
    [self.userPrefView addSubview:_aboutView];
    UISwipeGestureRecognizer* g = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipe:)];
    [g setDirection:UISwipeGestureRecognizerDirectionLeft];
    [self.userPrefView addGestureRecognizer:g];
    g = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipe:)];
    [g setDirection:UISwipeGestureRecognizerDirectionRight];
    [self.userPrefView addGestureRecognizer:g];
    
    self.userPrefView.userInteractionEnabled = YES;
    [self.userPrefView addSubview:_aboutView];
    [self.userPrefView bringSubviewToFront:_aboutView];
    
    UIView *doneContainer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 40)];
    UIButton* doneButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 80, 40)];
    [doneButton setTitle:@"Done" forState:UIControlStateNormal];
    doneButton.backgroundColor = [Helper sharedHelper].secondaryColor;
    doneButton.tintColor = [UIColor whiteColor];
    [doneButton addTarget:self action:@selector(dismissKeyboard:) forControlEvents:UIControlEventTouchDown];
    
    [doneContainer addSubview:doneButton];
    _t.inputAccessoryView = doneContainer;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    NSUInteger oldLength = [textView.text length];
    NSUInteger replacementLength = [text length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    
    BOOL returnKey = [text rangeOfString: @"\n"].location != NSNotFound;
    
    return newLength <= MAXLENGTH || returnKey;
}

-(void)dismissKeyboard:(UIButton*)btn {
    [_t resignFirstResponder];
    if([self.delegate respondsToSelector:@selector(onAboutTextChanged:)]) {
        [self.delegate onAboutTextChanged:_t.text];
    }
}

-(void)followersLabelTap:(id)sender {
    if([self.delegate respondsToSelector:@selector(onFollowersLabelTouched)]) {
        [self.delegate onFollowersLabelTouched];
    }
}

-(void)followingLabelTap:(id)sender {
    if([self.delegate respondsToSelector:@selector(onFollowingLabelTouched)]) {
        [self.delegate onFollowingLabelTouched];
    }
}

-(void)profilePictureTap:(id)sender{
    if([self.delegate respondsToSelector:@selector(onProfilePictureTouched)]) {
        [self.delegate onProfilePictureTouched];
    }
}

-(void)coverLongTap:(UILongPressGestureRecognizer*)g {
    if (g.state == UIGestureRecognizerStateBegan) {
        if([self.delegate respondsToSelector:@selector(onCoverPictureTouched)]) {
            [self.delegate onCoverPictureTouched];
        }
    }
}

-(void)setEditable:(BOOL)w {
    _t.editable = w;
    _t.delegate = nil;
    _t.inputAccessoryView = nil;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    self.actionButton.layer.cornerRadius = 4;
    self.actionButton.layer.borderWidth = 1;
    self.actionButton.layer.borderColor = [[UIColor whiteColor] CGColor];
    self.profileImageView.layer.cornerRadius = self.profileImageView.bounds.size.width / 2;
    self.profileImageView.layer.masksToBounds = YES;
    self.profileImageView.layer.borderWidth = 1;
    self.profileImageView.layer.borderColor = [[UIColor whiteColor] CGColor];
}


@end
