//
//  EditorChangeMethodViewController.m
//  Jestr
//
//  Created by Árpád Kiss on 2014.07.09..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import "EditorChangeMethodViewController.h"
#import "AVHexColor.h"
#import "EditorTagsViewController.h"

@interface EditorChangeMethodViewController ()

@end

@implementation EditorChangeMethodViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.sendView.backgroundColor = [AVHexColor colorWithHex:0xE81D60];
    self.postView.backgroundColor = [AVHexColor colorWithHex:0x28BBB9];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)postButtonPressed:(id)sender {
    [self performSegueWithIdentifier:@"ToEditorTagsFromChangeMethod" sender:nil];
}
- (IBAction)sendButtonPressed:(id)sender {
    [self performSegueWithIdentifier:@"ToFriendSelectorFromChangeMethod" sender:nil];
}

@end
