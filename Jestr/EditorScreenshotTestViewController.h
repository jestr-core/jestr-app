//
//  EditorScreenshotTestViewController.h
//  Jestr
//
//  Created by Árpád Kiss on 2014.07.09..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditorScreenshotTestViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (weak, nonatomic) IBOutlet UIImageView *layerImageView;

@property (strong, nonatomic) UIImage* bg;
@property (strong, nonatomic) UIImage* l;

@end
