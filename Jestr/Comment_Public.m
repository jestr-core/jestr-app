//
//  Comment_Public.m
//  Jestr
//
//  Created by Árpád Kiss on 2014.06.09..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import "Comment_Public.h"
#import "Comment_Preview.h"
#import "User_Public.h"


@implementation Comment_Public

@dynamic created_time;
@dynamic id;
@dynamic message;
@dynamic from;
@dynamic comment_preview;

@end
