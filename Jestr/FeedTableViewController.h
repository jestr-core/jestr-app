//
//  FeedTableViewController.h
//  Jestr
//
//  Created by Árpád Kiss on 2014.04.27..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

static NSString* const kReuseIdentifier = @"PostPublicTableViewCellPrototype";

@interface FeedTableViewController : UITableViewController

@end
