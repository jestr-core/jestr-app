//
//  PresetTableViewCell.m
//  Jestr
//
//  Created by Árpád Kiss on 2014.05.21..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import "PostPublicTableViewCell.h"
#import "Comment_Preview.h"
#import "Cool_Preview.h"
#import "User_Public.h"
#import "ILTranslucentView.h"
#import "UIImageView+AFNetworking.h"
#import "AVHexColor.h"
#import "Helper.h"

@implementation PostPublicTableViewCell {
    NSArray *_comments;
    BOOL _imageShown;
    CGFloat _animDuration;
    ILTranslucentView *_blurView;
    ProfileActionType _actionType;
    NSIndexPath* _path;
    UIImageView* _coolLayer;
}

- (void)awakeFromNib
{
    self.separatorInset = UIEdgeInsetsMake(0, 0, 0, self.bounds.size.width);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void)configureCellforType:(ProfileActionType)type
       withIndexPath:(NSIndexPath *)path
{
    _path = path;
    _blurView = [[ILTranslucentView alloc] initWithFrame:CGRectMake(0, 0, 320, 320)];
    _blurView.translucentAlpha = 0;
    _blurView.translucentStyle = UIBarStyleBlackTranslucent;
    _blurView.translucentTintColor = [UIColor clearColor];
    _blurView.backgroundColor = [UIColor clearColor];
    _imageShown = YES;
    _comments = [self.post.comments.data allObjects];
    
    _comments = [_comments sortedArrayUsingComparator:^NSComparisonResult(Comment_Public* obj1, Comment_Public* obj2) {
        if([obj1.created_time isEqualToNumber:obj2.created_time]) {
            return 0;
        } else if([obj1.created_time compare:obj2.created_time ] == NSOrderedAscending) {
            return -1;
        } else {
            return 1;
        }
    }];
    
    _animDuration = 0.25;
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(labelTap:)];
    
    self.commentsLabel.userInteractionEnabled = YES;
    self.coolLabel.userInteractionEnabled = YES;
    self.usernameLabel.userInteractionEnabled = YES;
    
    [self.commentsLabel addGestureRecognizer:tapGesture];
    
    tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(labelTap:)];
    
    [self.coolLabel addGestureRecognizer:tapGesture];
    
    tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(labelTap:)];
    
    [self.usernameLabel addGestureRecognizer:tapGesture];
    
    
    UITapGestureRecognizer *t = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleTap:)];
    [t setNumberOfTapsRequired:2];
    [self.backgroundImageView addGestureRecognizer:t];
    self.backgroundImageView.userInteractionEnabled = YES;
    
    [self configureCellLayout:type];
    
    _coolLayer = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 320)];
    _coolLayer.image = [UIImage imageNamed:@"cool"];
    _coolLayer.contentMode = UIViewContentModeCenter;
    _coolLayer.alpha = 0;
    [_backgroundImageView addSubview:_coolLayer];
}

-(void)doubleTap:(UITapGestureRecognizer*)g {
    if([self.delegate respondsToSelector:@selector(onDoubleTap:withSender:andIndexPath:)]) {
        [self animateCoolLayer];
        [self.delegate onDoubleTap:self.post withSender:self andIndexPath:_path];
    }
}

-(void)animateCoolLayer {
    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        _coolLayer.alpha = 0.4;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            _coolLayer.alpha = 0;
        } completion:^(BOOL finished) {
            
        }];
    }];
}

-(IBAction)labelTap:(UITapGestureRecognizer*)sender {
    NSLog(@"Tapped: %@", sender);
    if(sender.view == self.commentsLabel) {
        if([self.delegate respondsToSelector:@selector(onCommentsLabelTouched:withSender:andIndexPath:)])
        {
            [self.delegate onCommentsLabelTouched:self.post withSender:self andIndexPath:_path];
        }
    } else if( sender.view == self.coolLabel) {
        if([self.delegate respondsToSelector:@selector(onCoolersLabelTouched:withSender:andIndexPath:)])
        {
            [self.delegate onCoolersLabelTouched:self.post withSender:self andIndexPath:_path];
        }
    } else if( sender.view == self.usernameLabel) {
        if([self.delegate respondsToSelector:@selector(onUsernameLabelTouched:withSender:andIndexPath:)])
        {
            [self.delegate onUsernameLabelTouched:self.post withSender:self andIndexPath:_path];
        }
    }
}

#pragma mark tv delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(_comments.count > 0) {
        return _comments.count;
    } else {
        return 1;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(_comments.count == 0) {
        CommentPublicTableViewCell *cell = [[CommentPublicTableViewCell alloc] initWithFrame:CGRectMake(0, 0, 320, 74)];

        Comment_Public* c = [Comment_Public MR_createEntity];
        c.message = @"Be the first commenter";
        c.from.profile_picture = [[Helper sharedHelper].currentUser.profile objectForKey:@"picture"];
        
        [cell configureCell:c];
        return cell;
    }
    
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CommentPublicTableViewCell" owner:self options:nil];
    
    CommentPublicTableViewCell *cell = (CommentPublicTableViewCell*)[nib objectAtIndex:0];
    
    NSInteger idx = indexPath.row;
    
    Comment_Public *c = (Comment_Public*)_comments[idx];
    
    [cell configureCell:c];
    
    cell.delegate = self;
    
    return cell;
}

// row patch
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // 15-ös system font
    if(_comments.count > 0) {
    Comment_Public* c = (Comment_Public*)_comments[indexPath.row];
    
    CGRect r = [c.message boundingRectWithSize:CGSizeMake(236, 0)
                                       options:NSStringDrawingUsesLineFragmentOrigin
                                    attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15]}
                                       context:nil];
    
    return 74 + r.size.height - 21;
    } else {
        return 74;
    }
}

-(BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

-(void)configureCellLayout:(ProfileActionType)type {
    
    _actionType = type;
    if(_comments.count > 0) {
        [self.commentPreTableView reloadData];
    }
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
    NSString *imageURL = [@"http://api.jestr.me/"
                          stringByAppendingString:EMPTYIFNIL(                                                       [[self.post.images objectForKey:@"cropped"] valueForKey:@"standard"])];
    [self.backgroundImageView setImageWithURL:[NSURL URLWithString:imageURL]];
    
    NSString *profileImageURL = [@"http://api.jestr.me/"
                                 stringByAppendingString:EMPTYIFNIL(                                                       [self.post.author.profile_picture valueForKey:@"medium"])];
    
    [self.profileImageView setImageWithURL:[NSURL URLWithString:profileImageURL]];
    self.commentsLabel.text = [self.post.comments.count stringValue];
    self.coolLabel.text = [self.post.coolerz.count stringValue];
    self.dateLabel.text = [[Helper sharedHelper] dateDiff:[self.post.created_time doubleValue]];
    self.profileImageView.layer.cornerRadius = self.profileImageView.bounds.size.width / 2;
    self.profileImageView.layer.masksToBounds = YES;
    self.profileImageView.layer.borderWidth = 1;
    self.profileImageView.layer.borderColor = [[UIColor whiteColor] CGColor];
    
    self.profilePreviewActionButton.layer.borderWidth = 1;
    self.profilePreviewActionButton.layer.cornerRadius = 2;
    self.profilePreviewActionButton.layer.borderColor = [[UIColor whiteColor] CGColor];
    
    self.usernameLabel.text = self.post.author.username;
    
    if(type == friend) {
        [self.profilePreviewActionButton setTitle:@"unfollow" forState:UIControlStateNormal];
    } else if (type == unknown) {
        [self.profilePreviewActionButton setTitle:@"follow" forState:UIControlStateNormal];
    } else if (type == own) {
        [self.profilePreviewActionButton setTitle:@"DELETE" forState:UIControlStateNormal];
    }
    [self.profilePreviewActionButton sizeToFit];

    [self.commentButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.commentButton.backgroundColor = [AVHexColor colorWithHex:0xE81D60];
}

-(void)performSwipe:(UISwipeGestureRecognizer *)r {
    if (r.direction == UISwipeGestureRecognizerDirectionLeft) {
         NSLog(@"left");
         // komment
         if(_imageShown) {
             _imageShown = NO;
             [self.postPreviewView addSubview:_blurView];
            [UIView animateWithDuration:_animDuration animations:^{
                _blurView.translucentAlpha = 1;
                self.commentPreviewView.frame = CGRectMake(0, 0, 320, 320);
            }
            completion:^(BOOL finished) {
             
            }];
         } else {
             
             if(self.commentPreviewView.frame.origin.x == 0) {
                 return;
             }
             
             _imageShown = YES;
            [UIView animateWithDuration:_animDuration animations:^{
                _blurView.translucentAlpha = 0;
                 self.profilePreviewView.frame = CGRectMake(-320, 0, 320, 320);
            }
            completion:^(BOOL finished) {
                [_blurView removeFromSuperview];
            }];
         }
     } else {
         NSLog(@"right");
         if(_imageShown) {
             _imageShown = NO;
             [self.postPreviewView addSubview:_blurView];
             [UIView animateWithDuration:_animDuration animations:^{
                 _blurView.translucentAlpha = 1;
                 self.profilePreviewView.frame = CGRectMake(0, 0, 320, 320);
             }
             completion:^(BOOL finished) {
                                  
             }];
         } else {
             if(self.profilePreviewView.frame.origin.x == 0) {
                 return;
             }
             
             _imageShown = YES;
             [UIView animateWithDuration:_animDuration animations:^{
                 _blurView.translucentAlpha = 0;
                 self.commentPreviewView.frame = CGRectMake(320, 0, 320, 320);
             }
             completion:^(BOOL finished) {
                [_blurView removeFromSuperview];
             }];
         }
     }
}

-(void)resetCell {
    if(self.profilePreviewView.frame.origin.x == 0) {
        _imageShown = YES;
        [UIView animateWithDuration:_animDuration animations:^{
            _blurView.translucentAlpha = 0;
            self.commentPreviewView.frame = CGRectMake(320, 0, 320, 320);
        }
                         completion:^(BOOL finished) {
                             [_blurView removeFromSuperview];
                         }];
    } else if(self.commentPreviewView.frame.origin.x == 0) {
        _imageShown = YES;
        [UIView animateWithDuration:_animDuration animations:^{
            _blurView.translucentAlpha = 0;
            self.profilePreviewView.frame = CGRectMake(-320, 0, 320, 320);
        }
                         completion:^(BOOL finished) {
                             [_blurView removeFromSuperview];
                         }];
    }
}

-(void)forceResetCell {
    self.backgroundImageView.image = nil;
    self.profileImageView.image = nil;
    _imageShown = YES;
    [_blurView removeFromSuperview];
    self.profilePreviewView.frame = CGRectMake(-320, 0, 320, 320);
    self.commentPreviewView.frame = CGRectMake(320, 0, 320, 320);
}

#pragma mark delegate methods
- (IBAction)onActionButtonPressed:(id)sender {
    if([self.delegate respondsToSelector:@selector(onProfileAction:withActionType:andSender:andIndexPath:)])
    {
        [self.delegate onProfileAction:self.post withActionType:_actionType andSender:self andIndexPath:_path];
    }
}

- (IBAction)onCommentButtonPressed:(id)sender {
    if([self.delegate respondsToSelector:@selector(onCommentButtonPress:withSender:andIndexPath:)])
    {
        [self.delegate onCommentButtonPress:self.post withSender:self andIndexPath:_path];
    }
}

#pragma mark commentpublictableviewcell 

-(void)onUsernameLabelTapped:(Comment_Public *)comment {
    if([self.delegate respondsToSelector:@selector(onCommentUsernameLabelTouched:withPost:withSender:andIndexPath:)]) {
        [self.delegate onCommentUsernameLabelTouched:comment withPost:self.post withSender:self andIndexPath:_path];
    }
}


@end
