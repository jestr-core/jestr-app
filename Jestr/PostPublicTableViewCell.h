//
//  PresetTableViewCell.h
//  Jestr
//
//  Created by Árpád Kiss on 2014.05.21..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Post_Public.h"
#import "Comment_Public.h"
#import "CommentPublicTableViewCell.h"

typedef NS_ENUM(NSUInteger, ProfileActionType) {
    friend,
    unknown,
    own
};

@interface PostPublicTableViewCell : UITableViewCell <
    UITableViewDelegate,
    UITableViewDataSource,
    CommentPublicTableViewCellDelegate
>

@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (weak, nonatomic) IBOutlet UITableView *commentPreTableView;
@property (weak, nonatomic) IBOutlet UIView *commentPreviewView;
@property (weak, nonatomic) IBOutlet UIView *profilePreviewView;
@property (weak, nonatomic) IBOutlet UIButton *profilePreviewActionButton;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UILabel *commentsLabel;
@property (weak, nonatomic) IBOutlet UILabel *coolLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (nonatomic, assign) Post_Public *post;
@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;
@property (weak, nonatomic) IBOutlet UIView *postPreviewView;
@property (weak, nonatomic) IBOutlet UIButton *commentButton;

@property (nonatomic, assign) id delegate;

-(void)configureCellforType:(ProfileActionType)type
       withIndexPath:(NSIndexPath*)path;
-(void)performSwipe:(UIGestureRecognizer *)r;
-(void)resetCell;
-(void)forceResetCell;

@end

@protocol PostPublicTableViewCellDelegate

-(void)onCommentButtonPress:(Post_Public*)post
                 withSender:(PostPublicTableViewCell*)cell
               andIndexPath:(NSIndexPath*)path;
-(void)onProfileAction:(Post_Public*)post
        withActionType:(ProfileActionType)type
             andSender:(PostPublicTableViewCell*)cell
          andIndexPath:(NSIndexPath*)path;
-(void)onCommentsLabelTouched:(Post_Public*)post
                   withSender:(PostPublicTableViewCell*)cell
                 andIndexPath:(NSIndexPath*)path;
-(void)onCoolersLabelTouched:(Post_Public*)post
                  withSender:(PostPublicTableViewCell*)cell
                andIndexPath:(NSIndexPath*)path;

-(void)onUsernameLabelTouched:(Post_Public*)post
                  withSender:(PostPublicTableViewCell*)cell
                andIndexPath:(NSIndexPath*)path;
-(void)onDoubleTap:(Post_Public*)post
        withSender:(PostPublicTableViewCell*)cell
      andIndexPath:(NSIndexPath*)path;

-(void)onCommentUsernameLabelTouched:(Comment_Public*)comment
                            withPost:(Post_Public*)post
                          withSender:(PostPublicTableViewCell*)cell
                        andIndexPath:(NSIndexPath*)path;

@end
