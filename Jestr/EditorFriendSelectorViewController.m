//
//  EditorFriendSelectorViewController.m
//  Jestr
//
//  Created by Árpád Kiss on 2014.07.11..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import "EditorFriendSelectorViewController.h"
#import "APIAgent.h"
#import "Helper.h"

@interface EditorFriendSelectorViewController () {
    NSArray* _followers;
    NSMutableSet* _recipents;
    NSManagedObjectContext *_tmpContext;
}

@end

@implementation EditorFriendSelectorViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _tmpContext = [NSManagedObjectContext MR_contextWithParent:[NSManagedObjectContext MR_contextForCurrentThread]];
    
    UINib* nib = [UINib nibWithNibName:@"FollowerSelectionTableViewCell" bundle:nil];
    [self.followersTableView registerNib:nib forCellReuseIdentifier:kFollowerCellReuseIdentifier];
    
    _recipents = [[NSMutableSet alloc] init];
    
    self.okButton.backgroundColor = [Helper sharedHelper].secondaryColor;
    
    [self fetchFollowers];
}

-(void)fetchFollowers {
    [[APIAgent sharedAgent] getFromEndpoint:@"users/me/followers" onSuccess:^(id responseObject) {
        NSLog(@"DATA: %@", responseObject);
        
        if(![responseObject isEqual:[NSNull null]]) {
            
            NSMutableSet *tmp = [[NSMutableSet alloc] init];
            
            [responseObject enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                [tmp addObject:[User_Public MR_importFromObject:obj inContext:_tmpContext]];
            }];
            
            _followers = [tmp allObjects];
            
            [self.followersTableView reloadData];
        }
        
    } onFail:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _followers.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    FollowerSelectionTableViewCell* cell = [self.followersTableView dequeueReusableCellWithIdentifier:kFollowerCellReuseIdentifier];
    cell.delegate = self;
    [cell configureCell:_followers[indexPath.row]];
    return cell;
}

-(BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

-(void)followerSelected:(User_Public *)user {
    [_recipents addObject:user];
}

-(void)followerDeselected:(User_Public *)user {
    [_recipents removeObject:user];
}

- (IBAction)composeMessage:(id)sender {
    NSLog(@"%@", _recipents);
    
    NSMutableArray* _recipentIds = [[NSMutableArray alloc] init];
    
    [_recipents enumerateObjectsUsingBlock:^(User_Public* obj, BOOL *stop) {
        [_recipentIds addObject:obj.id];
    }];
    
    self.okButton.enabled = NO;
    NSArray* images = [[NSArray alloc] initWithObjects:
                       @{
                         @"name": @"background",
                         @"image": [Helper sharedHelper].editorBackground
                         },
                       @{
                         @"name": @"layer",
                         @"image": [Helper sharedHelper].editorLayer
                         },
                       @{
                         @"name": @"cropped",
                         @"image": [Helper sharedHelper].editorThumbnail
                         }
                       , nil];
    [[APIAgent sharedAgent] postToEndpoint:@"users/me/messages"
                                withParams:@{@"recipents": _recipentIds} withImages:images
                                 onSuccess:^(id responseObject) {
                                     if(![responseObject isEqual:[NSNull null]]) {
                                         // sikeres
                                         [[NSNotificationCenter defaultCenter] postNotificationName:@"EditorNavigationControllerDismissed"
                                                                                             object:nil
                                                                                           userInfo:nil];
                                         [self.navigationController dismissViewControllerAnimated:YES completion:^{
                                             
                                         }];
                                         [Helper sharedHelper].editorThumbnail = nil;
                                         [Helper sharedHelper].editorLayer = nil;
                                         [Helper sharedHelper].editorBackground = nil;
                                     }
                                     
                                 } onFail:
     ^(NSError *error) {
         NSLog(@"%@",error);
     }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
