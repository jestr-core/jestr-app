//
//  CommentPublicTableViewCell.h
//  Jestr
//
//  Created by Árpád Kiss on 2014.06.08..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Comment_Public.h"

@interface CommentPublicTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *profilePictureImageView;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@property (nonatomic, assign) id delegate;

-(void)configureCell:(Comment_Public*)comment;

@end

@protocol CommentPublicTableViewCellDelegate

-(void)onUsernameLabelTapped:(Comment_Public*)comment;

@end
