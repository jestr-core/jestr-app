//
//  SearchViewController.m
//  Jestr
//
//  Created by Árpád Kiss on 2014.07.03..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import "SearchViewController.h"
#import "APIAgent.h"
#import "User_Public.h"
#import "Post_Public.h"
#import "CoolerTableViewCell.h"
#import "PostPublicTableViewCell.h"
#import "Helper.h"
#import "CommentViewController.h"
#import "CoolersViewController.h"
#import "ProfileFeedViewController.h"
#import "Comment_Preview.h"
#import "Comment_Public.h"

typedef NS_ENUM(NSUInteger, SearchActionType) {
    user,
    hashtag
};

@interface SearchViewController () {
    SearchActionType _type;
    NSArray* _posts;
    NSArray* _users;
    NSManagedObjectContext* _tmpContext;
    Post_Public* _tmpPost;
    NSIndexPath* _tmpIndexPath;
    int _page;
}

@end

@implementation SearchViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _type = user;

    _page = 0;
    
    _tmpContext = [NSManagedObjectContext MR_contextWithParent:[NSManagedObjectContext MR_contextForCurrentThread]];
    
    // Do any additional setup after loading the view.
    [self.searchBar becomeFirstResponder];
    
    UINib* nib = [UINib nibWithNibName:@"CoolerTableViewCell" bundle:nil];
    [self.resultsTableView registerNib:nib forCellReuseIdentifier:kCoolerCellReuseIdentifier];
    
    nib = [UINib nibWithNibName:@"PostPublicTableViewCell" bundle:nil];
    
    [self.resultsTableView registerNib:nib forCellReuseIdentifier:kFeedCellReuseIdentifier];

}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    UISwipeGestureRecognizer *r = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(didSwipe:)];
    [r setDirection:UISwipeGestureRecognizerDirectionRight];
    [self.resultsTableView addGestureRecognizer:r];
    r = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(didSwipe:)];
    [r setDirection:UISwipeGestureRecognizerDirectionLeft];
    [self.resultsTableView addGestureRecognizer:r];
    [self checkTempPost];
}

- (void)didSwipe:(UIGestureRecognizer *)gestureRecognizer {
    CGPoint swipeLocation = [gestureRecognizer locationInView:self.resultsTableView];
    NSIndexPath *swipedIndexPath = [self.resultsTableView indexPathForRowAtPoint:swipeLocation];
    
    if([[self.resultsTableView cellForRowAtIndexPath:swipedIndexPath] isKindOfClass:[PostPublicTableViewCell class]]){
    
        PostPublicTableViewCell* swipedCell = (PostPublicTableViewCell*)[self.resultsTableView cellForRowAtIndexPath:swipedIndexPath];
    
        [swipedCell performSwipe:gestureRecognizer];
    
        NSLog(@"triggered swipe on %@", swipedCell);
        NSLog(@"%@", gestureRecognizer);
    }
}

-(void)checkTempPost {
    if(_tmpPost != nil) {
        NSString* postEndpoint = [@"posts/" stringByAppendingString:_tmpPost.id.description];
        [[APIAgent sharedAgent] getFromEndpoint:postEndpoint onSuccess:^(id responseObject) {
            NSDictionary* r = (NSDictionary*)responseObject;
            NSDictionary* cl = [r objectForKey:@"comments"];
            
            Comment_Preview* cp = [Comment_Preview MR_createInContext:_tmpContext];
            
            NSArray* comments = [cl objectForKey:@"data"];
            
            cp.count = [cl objectForKey:@"count"];
            cp.data = [NSSet new];
            
            NSMutableSet* tSet = [[NSMutableSet alloc] init];
            
            [comments enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                NSDictionary* current = (NSDictionary*)obj;
                NSDictionary* from = [current objectForKey:@"from"];
                
                Comment_Public* cl = [Comment_Public MR_createInContext:_tmpContext];
                cl.id = [current objectForKey:@"id"];
                cl.message = [current valueForKey:@"message"];
                
                cl.created_time = [current objectForKey:@"created_time"];
                
                User_Public *p = [User_Public MR_createInContext:_tmpContext];
                p.id = [from objectForKey:@"id"];
                p.username = [from valueForKey:@"username"];
                p.profile_picture = NILIFNULL([from objectForKey:@"profile_picture"]);
                
                cl.from = p;
                
                [tSet addObject:cl];
            }];
            
            cp.data = [NSSet setWithSet:tSet];
            
            _tmpPost.comments = cp;
            
            [self.resultsTableView reloadRowsAtIndexPaths:@[_tmpIndexPath] withRowAnimation:UITableViewRowAnimationNone];
            
        } onFail:^(id error) {
            
        }];
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(_type == user) {
        return _users.count;
    } else {
        return _posts.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(_type == user) {
        CoolerTableViewCell* cell = [self.resultsTableView dequeueReusableCellWithIdentifier:kCoolerCellReuseIdentifier];
        [cell configureCell:_users[indexPath.row]];
        
        return cell;
    } else {
        PostPublicTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kFeedCellReuseIdentifier];
        
        [cell forceResetCell];
        NSInteger idx = indexPath.row;
        
        Post_Public *row = _posts[idx];
        
        cell.post = row;
        
        ProfileActionType type = ([row.author.id isEqual:[Helper sharedHelper].currentUser.id]) ? own : friend;
        
        [cell configureCellforType:type withIndexPath:indexPath];
        
        cell.delegate = self;
        
        return cell;
    }
}

#pragma mark SearchBarDelegate
- (void)searchBar:(UISearchBar *)searchBar selectedScopeButtonIndexDidChange:(NSInteger)selectedScope {
    if(selectedScope == 0) {
        _type = user;
    } else {
        _type = hashtag;
    }
    [self.resultsTableView reloadData];
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    NSString* k = searchBar.text;
    k = [k stringByTrimmingCharactersInSet:
         [NSCharacterSet whitespaceCharacterSet]];
    [self performSearch:k];
}

-(void)loadUsers:(NSString*)keyword {
    NSString* endpoint = @"users/search";
    NSDictionary* data = @{
                           @"keyword": keyword
                           };
    [[APIAgent sharedAgent] postToEndpoint:endpoint withParams:data onSuccess:^(NSArray* responseObject) {
        
        if(![responseObject isEqual:[NSNull null]]) {
            
            NSMutableSet* tmp = [[NSMutableSet alloc] init];
            
            [responseObject enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                [tmp addObject:[User_Public MR_importFromObject:obj inContext:_tmpContext]];
            }];
            
            _users = [tmp allObjects];
            
            [self.resultsTableView reloadData];
            
            [self.searchBar resignFirstResponder];
        }
        
    } onFail:^(id error) {
        NSLog(@"%@",error);
    }];

}

//-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
//    if(indexPath.row == _posts.count - 1) {
//        _page = _page + 1;
//        [self loadPosts:self.searchBar.text];
//    }
//}

-(void)loadPosts:(NSString*)keyword
{
    NSString* endpoint = [[[@"tags/" stringByAppendingString:keyword] stringByAppendingString:@"/posts?page="] stringByAppendingFormat:@"%i", _page];
    
    [[APIAgent sharedAgent] getFromEndpoint:endpoint onSuccess:^(NSArray* responseObject) {
        
        if(![responseObject isEqual:[NSNull null]]) {
            
            NSMutableSet* tmp = [[NSMutableSet alloc] init];
            
            [responseObject enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                
                [tmp addObject:[Post_Public MR_importFromObject:obj inContext:_tmpContext]];
            }];
            
            _posts = [tmp allObjects];
            
            [self.resultsTableView reloadData];
            
            [self.searchBar resignFirstResponder];
            
        } else {
            _page = (_page == 0) ? 0 : _page - 1;
        }
    } onFail:^(id error) {
        NSLog(@"%@",error);
    }];
}

-(void)performSearch:(NSString*)keyword {
    if(_type == user) {
        [self loadUsers:keyword];
    } else {
        [self loadPosts:keyword];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(_type == user) {
        return 60;
    } else {
        return 320;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(_type == user) {
        [self performSegueWithIdentifier:@"ToProfileFeedViewFromSearch" sender:_users[indexPath.row]];
    }
}

#pragma mark delegate methods

-(void)onCommentButtonPress:(Post_Public*)post
                 withSender:(PostPublicTableViewCell*)cell
               andIndexPath:(NSIndexPath*)path
{
    _tmpIndexPath = path;
    _tmpPost = post;
    [cell resetCell];
    [self performSegueWithIdentifier:@"ToCommentFromSearch" sender:nil];
}

-(void)onProfileAction:(Post_Public*)post
        withActionType:(ProfileActionType)type
             andSender:(PostPublicTableViewCell*)cell
          andIndexPath:(NSIndexPath*)path
{
    
}

-(void)onCommentsLabelTouched:(Post_Public*)post
                   withSender:(PostPublicTableViewCell*)cell
                 andIndexPath:(NSIndexPath*)path
{
    NSLog(@"comments label tapped.");
    _tmpIndexPath = path;
    _tmpPost = post;
    [cell resetCell];
    [self performSegueWithIdentifier:@"ToCommentFromSearch" sender:nil];
    
}

-(void)onCoolersLabelTouched:(Post_Public*)post
                  withSender:(PostPublicTableViewCell*)cell
                andIndexPath:(NSIndexPath*)path
{
    NSLog(@"coolers label tapped.");
    _tmpIndexPath = path;
    _tmpPost = post;
    [cell resetCell];
    [self performSegueWithIdentifier:@"ToCoolersFromSearch" sender:nil];
}

-(void)onUsernameLabelTouched:(Post_Public*)post
                   withSender:(PostPublicTableViewCell*)cell
                 andIndexPath:(NSIndexPath*)path {
    NSLog(@"username label tapped.");
    _tmpIndexPath = path;
    _tmpPost = post;
    [cell resetCell];
    [self performSegueWithIdentifier:@"ToProfileFeedViewFromSearch" sender:nil];
    
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    if([segue.identifier isEqual:@"ToCommentFromSearch"]) {
        CommentViewController* c = (CommentViewController*)segue.destinationViewController;
        c.post = _tmpPost;
    } else if([segue.identifier isEqual:@"ToProfileFeedViewFromSearch"]) {
        ProfileFeedViewController* c = (ProfileFeedViewController*)segue.destinationViewController;
        if(!sender) {
            c.public = _tmpPost.author;
        } else {
            c.public = sender;
        }
    } else if([segue.identifier isEqual:@"ToCoolersFromSearch"]) {
        CoolersViewController* c = (CoolersViewController*)segue.destinationViewController;
        c.preview = _tmpPost.coolerz;
    }
}


@end
