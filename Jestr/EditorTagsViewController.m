//
//  EditorTagsViewController.m
//  Jestr
//
//  Created by Árpád Kiss on 2014.07.09..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import "EditorTagsViewController.h"
#import "AVHexColor.h"
#import "APIAgent.h"
#import "Helper.h"

@interface EditorTagsViewController ()

@end

@implementation EditorTagsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.thumbnailImageView.layer.minificationFilter = kCAFilterTrilinear;
    self.thumbnailImageView.image = [Helper sharedHelper].editorThumbnail;
    
    
    
//    UIImageView* i = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.thumbnailImageView.frame.size.width, self.thumbnailImageView.frame.size.height)];
//    [i setImage:self.layer];
//    [i setContentMode:UIViewContentModeScaleToFill];
//    [self.thumbnailImageView addSubview:i];
    
    
    self.captionTextView.keyboardType = UIKeyboardTypeTwitter;

    
    NSLog(@"f: %@", NSStringFromCGRect(self.thumbnailImageView.frame));
    NSLog(@"b: %@", NSStringFromCGRect(self.thumbnailImageView.bounds));
    
//    NSLog(@"f: %@", NSStringFromCGRect(i.frame));
//    NSLog(@"b: %@", NSStringFromCGRect(i.bounds));

    UIView *doneContainer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 40)];
    UIButton* doneButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 80, 40)];
    [doneButton setTitle:@"Done" forState:UIControlStateNormal];
    doneButton.backgroundColor = [Helper sharedHelper].secondaryColor;
    doneButton.tintColor = [UIColor whiteColor];
    [doneButton addTarget:self action:@selector(dismissKeyboard:) forControlEvents:UIControlEventTouchDown];
    
    [doneContainer addSubview:doneButton];
    
    self.okButton.backgroundColor = [Helper sharedHelper].secondaryColor;
    self.captionContainerView.backgroundColor = [Helper sharedHelper].secondaryColor;
    
    self.captionTextView.inputAccessoryView = doneContainer;
}

-(void)dismissKeyboard:(UIButton*)btn {
    [self.captionTextView resignFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)composePost:(id)sender {
    self.okButton.enabled = NO;
    NSArray* images = [[NSArray alloc] initWithObjects:
                       @{
                         @"name": @"background",
                         @"image": [Helper sharedHelper].editorBackground
                         },
                       @{
                         @"name": @"layer",
                         @"image": [Helper sharedHelper].editorLayer
                         },
                       @{
                         @"name": @"cropped",
                         @"image": [Helper sharedHelper].editorThumbnail
                         }
                       , nil];
    [[APIAgent sharedAgent] postToEndpoint:@"users/me/posts"
withParams:@{@"caption": self.captionTextView.text} withImages:images
onSuccess:^(id responseObject) {
    if(![responseObject isEqual:[NSNull null]]) {
        // sikeres
        [[NSNotificationCenter defaultCenter] postNotificationName:@"EditorNavigationControllerDismissed"
                                                            object:nil
                                                          userInfo:nil];
        [self.navigationController dismissViewControllerAnimated:YES completion:^{
            
        }];
        [Helper sharedHelper].editorThumbnail = nil;
        [Helper sharedHelper].editorLayer = nil;
        [Helper sharedHelper].editorBackground = nil;
    }
    
} onFail:
    ^(NSError *error) {
        NSLog(@"%@",error);
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
