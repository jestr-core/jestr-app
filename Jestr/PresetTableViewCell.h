//
//  PresetTableViewCell.h
//  Jestr
//
//  Created by Árpád Kiss on 2014.06.06..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PresetTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;

-(void)configureCell:(UIImage*)filteredImage withLabel:(NSString*)label andAttributes:(NSDictionary*)attr;

@end
