//
//  FeedTableViewController.m
//  Jestr
//
//  Created by Árpád Kiss on 2014.04.27..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import "FeedTableViewController.h"
#import "LoginViewController.h"
#import "AVHexColor.h"


@interface FeedTableViewController ()

@end

@implementation FeedTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    //[self.navigationController  setNavigationBarHidden:YES];
    //self.view.backgroundColor = [AVHexColor colorWithHex:0xEF3973];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 320;
}

@end
