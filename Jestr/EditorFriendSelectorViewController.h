//
//  EditorFriendSelectorViewController.h
//  Jestr
//
//  Created by Árpád Kiss on 2014.07.11..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FollowerSelectionTableViewCell.h"

@interface EditorFriendSelectorViewController : UIViewController <
    UITableViewDataSource,
    UITableViewDelegate,
    FollowerSelectionDelegate
>

@property (weak, nonatomic) IBOutlet UITableView *followersTableView;
@property (weak, nonatomic) IBOutlet UIButton *okButton;

@end
