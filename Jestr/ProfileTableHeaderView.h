//
//  ProfileTableHeaderView.h
//  Jestr
//
//  Created by Árpád Kiss on 2014.06.09..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User_Profile.h"

@interface ProfileTableHeaderView : UIView

@property (weak, nonatomic) IBOutlet UIImageView *coverImageView;
@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;
@property (weak, nonatomic) IBOutlet UIButton *actionButton;
@property (weak, nonatomic) IBOutlet UILabel *followingLabel;
@property (weak, nonatomic) IBOutlet UILabel *followersLabel;
@property (weak, nonatomic) IBOutlet UILabel *imagesLabel;
@property (weak, nonatomic) IBOutlet UIView *userPrefView;

@property (weak, nonatomic) IBOutlet UIView *followingContainerView;
@property (weak, nonatomic) IBOutlet UIView *followersContainerView;
@property (weak, nonatomic) IBOutlet UIView *postsContainerView;

@property (nonatomic, assign) id delegate;

-(void)configureView:(User_Profile*)profile;
-(void)setEditable:(BOOL)w;

@end

@protocol ProfileTableHeaderViewDelegate

-(void)onFollowersLabelTouched;
-(void)onFollowingLabelTouched;
-(void)onProfilePictureTouched;
-(void)onCoverPictureTouched;
-(void)onAboutTextChanged:(NSString*)about;

@end