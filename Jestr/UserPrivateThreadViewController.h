//
//  UserPrivateThreadViewController.h
//  Jestr
//
//  Created by Árpád Kiss on 2014.07.02..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User_Public.h"

@interface UserPrivateThreadViewController : UIViewController <
    UITableViewDelegate,
    UITableViewDataSource
>

@property (weak, nonatomic) IBOutlet UITableView *messagesTableView;
@property (nonatomic, strong) User_Public* friend;

@end
