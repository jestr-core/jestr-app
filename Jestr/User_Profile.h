//
//  User_Profile.h
//  Jestr
//
//  Created by Árpád Kiss on 2014.07.15..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class User_Public;

@interface User_Profile : NSManagedObject

@property (nonatomic, retain) NSNumber * id;
@property (nonatomic, retain) NSNumber * private;
@property (nonatomic, retain) NSDictionary* profile;
@property (nonatomic, retain) NSDictionary* stats;
@property (nonatomic, retain) NSString * username;
@property (nonatomic, retain) User_Public *user_public;
@property (nonatomic, retain) NSSet *followers;
@property (nonatomic, retain) NSSet *following;

@end

@interface User_Profile (CoreDataGeneratedAccessors)

- (void)addFollowersObject:(User_Public *)value;
- (void)removeFollowersObject:(User_Public *)value;
- (void)addFollowers:(NSSet *)values;
- (void)removeFollowers:(NSSet *)values;

- (void)addFollowingObject:(User_Public *)value;
- (void)removeFollowingObject:(User_Public *)value;
- (void)addFollowing:(NSSet *)values;
- (void)removeFollowing:(NSSet *)values;

@end
