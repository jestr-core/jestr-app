//
//  ProfileFeedViewController.h
//  Jestr
//
//  Created by Árpád Kiss on 2014.06.09..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User_Public.h"
#import "PostPublicTableViewCell.h"
#import "ProfileTableHeaderView.h"

@interface ProfileFeedViewController : UIViewController <//NSFetchedResultsControllerDelegate,
UITableViewDelegate,
UITableViewDataSource,
PostPublicTableViewCellDelegate,
ProfileTableHeaderViewDelegate,
UIImagePickerControllerDelegate,
UINavigationControllerDelegate
>

@property (weak, nonatomic) IBOutlet UITableView *userPostsTableView;
@property (strong, nonatomic) User_Public * public;

@end
