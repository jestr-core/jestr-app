//
//  ColorPicker.h
//  Jestr
//
//  Created by Árpád Kiss on 2014.07.08..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ColorPicker : UIControl

@property (nonatomic, strong) UIColor* currentColor;
@property (nonatomic, assign) id delegate;

@end

@protocol ColorPickerDelegate

-(void)colorPicker:(ColorPicker*)colorpicker
    didChangeColor:(UIColor*)color;

@end
