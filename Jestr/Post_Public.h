//
//  Post_Public.h
//  Jestr
//
//  Created by Árpád Kiss on 2014.07.22..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Comment_Preview, Cool_Preview, User_Public;

@interface Post_Public : NSManagedObject

@property (nonatomic, retain) NSNumber * created_time;
@property (nonatomic, retain) id hashtags;
@property (nonatomic, retain) NSNumber * id;
@property (nonatomic, retain) id images;
@property (nonatomic, retain) NSNumber *type; // 1: feed, 2: trending
@property (nonatomic, retain) User_Public *author;
@property (nonatomic, retain) Comment_Preview *comments;
@property (nonatomic, retain) Cool_Preview *coolerz;

@end
