//
//  PrepareViewController.m
//  Jestr
//
//  Created by Árpád Kiss on 2014.04.27..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import "PrepareViewController.h"
#import "APIAgent.h"
#import "Helper.h"

@interface PrepareViewController () {
    UIImage *_background;
    UIStoryboard *_sb;
}
@property (strong, nonatomic) IBOutlet UIImageView *backgroundView;

@end

@implementation PrepareViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

    }
    return self;
}

- (void)viewDidLoad
{
    _sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

    NSString *launchImage;
    if  ((UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) &&
         ([UIScreen mainScreen].bounds.size.height > 480.0f)) {
        launchImage = @"LaunchImage-700-568h";
    } else {
        launchImage = @"LaunchImage-700";
    }
    _background = [UIImage imageNamed:launchImage];
    [self.backgroundView setImage: _background];
    NSLog(@"mkay");
    [super viewDidLoad];
}

- (void)viewDidAppear:(BOOL)animated{
    if(![Helper sharedHelper].currentUser || ![[APIAgent sharedAgent] isUserHaveSession]) {
       [self loadNavControllerWithPrefix:@"LoginNavControllerID"];
    } else {
       [self loadNavControllerWithPrefix:@"ApplicationNavControllerID"];
    }
}

- (void)loadNavControllerWithPrefix:(NSString*)prefix {
    self.currentNavController = [_sb instantiateViewControllerWithIdentifier:prefix];
    self.currentNavController.navigationBar.translucent = NO;
    [self.currentNavController setModalPresentationStyle:UIModalPresentationFullScreen];
    [self presentViewController:self.currentNavController
                       animated:YES
                     completion:nil];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
