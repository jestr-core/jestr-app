//
//  NotifUserInteractionTableViewCell.h
//  Jestr
//
//  Created by Árpád Kiss on 2014.07.03..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User_Public.h"
#import "Notification.h"

@interface NotifUserInteractionTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;
@property (weak, nonatomic) IBOutlet UIButton *actionButton;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (assign, nonatomic) id delegate;

-(void)configureCell:(Notification*)notif withMessage:(NSString*)message;

@end

@protocol NotifUserInteractionDelegate <NSObject>

-(void)onButtonClick:(Notification*)notif;

@end