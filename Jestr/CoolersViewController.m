//
//  CoolersViewController.m
//  Jestr
//
//  Created by Árpád Kiss on 2014.07.01..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import "CoolersViewController.h"
#import "CoolerTableViewCell.h"
#import "Helper.h"
#import "APIAgent.h"

@interface CoolersViewController () {
    NSArray* _users;
    NSString* _endpoint;
    User_Public* _user;
}

@end

@implementation CoolersViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Coolerz";
    // Do any additional setup after loading the view.
    UINib* nib = [UINib nibWithNibName:@"CoolerTableViewCell" bundle:nil];
    [self.userListTableView registerNib:nib forCellReuseIdentifier:kCoolerCellReuseIdentifier];
    
    NSLog(@"%@", self.preview.data);
    
    _users = [self.preview.data allObjects];
    
    //NSString* q = [[] stringByAppendingString:@" == %@"];
    
    NSPredicate* p =  [NSPredicate predicateWithFormat: @"id == %@", [Helper sharedHelper].currentUser.id];
    
    NSArray* user = [_users filteredArrayUsingPredicate:p];
    
    if(user.count > 0){
        _user = user[0];
        UIBarButtonItem *button = [[UIBarButtonItem alloc] initWithTitle:@"Uncool" style:UIBarButtonItemStyleBordered target:self action:@selector(onUncoolPressed)];
        self.navigationItem.rightBarButtonItem = button;
    }
    
    _endpoint = [[@"posts/" stringByAppendingString:self.postId.description] stringByAppendingString:@"/coolers"];
    
    [self.userListTableView reloadData];
}

-(void)onUncoolPressed {
    [[APIAgent sharedAgent] deleteFromEndpoint:_endpoint withParams:@{} onSuccess:^(id responseObject) {
        NSMutableSet* t = (NSMutableSet*)self.preview.data;
        [t removeObject:_user];
        _users = [t allObjects];
        
        self.preview.data = (NSSet*)t;
        
        [self.userListTableView reloadData];
        self.navigationItem.rightBarButtonItem = nil;
    } onFail:^(id error) {
        
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _users.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    CoolerTableViewCell* cell = [self.userListTableView dequeueReusableCellWithIdentifier:kCoolerCellReuseIdentifier];
    if(!cell) {
        cell = [[CoolerTableViewCell alloc] init];
    }
    
    [cell configureCell:_users[indexPath.row]];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

-(BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath{
    return NO;
}


@end
