//
//  CommentPublicTableViewCell.m
//  Jestr
//
//  Created by Árpád Kiss on 2014.06.08..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import "CommentPublicTableViewCell.h"
#import "UIImageView+AFNetworking.h"
#import "User_Public.h"
#import "Helper.h"

@implementation CommentPublicTableViewCell {
    Comment_Public* _c;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configureCell:(Comment_Public*)c
{
    _c = c;
    
    self.usernameLabel.text = _c.from.username;
    self.dateLabel.text = [[Helper sharedHelper] dateDiff:[_c.created_time doubleValue]];
    self.messageLabel.text = _c.message;
    
    [self.profilePictureImageView setImageWithURL:[NSURL URLWithString:
                                     [@"http://api.jestr.me/" stringByAppendingString:EMPTYIFNIL(
                                                                                                 [c.from.profile_picture valueForKey:@"medium"]
                                                                                                 )]] placeholderImage:nil];
    
    UITapGestureRecognizer* g = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onLabelTapped:)];
    self.usernameLabel.userInteractionEnabled = YES;
    [self.usernameLabel addGestureRecognizer:g];
}

-(void)onLabelTapped:(UITapGestureRecognizer*)g {
    if([self.delegate respondsToSelector:@selector(onUsernameLabelTapped:)]) {
        [self.delegate onUsernameLabelTapped:_c];
    }
}

-(void)drawRect:(CGRect)rect {
    self.profilePictureImageView.layer.cornerRadius = 25;
    self.profilePictureImageView.layer.masksToBounds = YES;
    self.profilePictureImageView.layer.borderWidth = 1;
    self.profilePictureImageView.layer.borderColor = [[UIColor whiteColor] CGColor];
    
    self.messageLabel.numberOfLines = 0;
    self.messageLabel.lineBreakMode = NSLineBreakByWordWrapping;
    
    CGRect r = [_c.message boundingRectWithSize:CGSizeMake(236, 0)
                                       options:NSStringDrawingUsesLineFragmentOrigin
                                    attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15]}
                                       context:nil];
    
    self.messageLabel.frame = CGRectMake(self.messageLabel.frame.origin.x, self.messageLabel.frame.origin.y, self.messageLabel.frame.size.width, r.size.height);
}

@end
