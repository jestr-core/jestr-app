//
//  ProfileFeedViewController.m
//  Jestr
//
//  Created by Árpád Kiss on 2014.06.09..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import "ProfileFeedViewController.h"
#import "Post_Public.h"
#import "Comment_Public.h"
#import "Comment_Preview.h"
#import "Helper.h"
#import "APIAgent.h"
#import "ILTranslucentView.h"
#import "User_Profile.h"
#import "CommentViewController.h"
#import "CoolersViewController.h"
#import "RelationViewController.h"
#import "ProfileSettingsViewController.h"

@interface ProfileFeedViewController () {
    Post_Public* _tmpPost;
    NSIndexPath* _tmpIndexPath;
    NSArray* _posts;
    ProfileActionType _type;
    NSManagedObjectContext* _tmpContext;
    NSPredicate* _p;
    ILTranslucentView* _blurView;
    User_Profile *_profile;
    User_Public* _upublic;
    ProfileTableHeaderView* _profileHeaderView;
    UIImagePickerController *_imgPicker;
    UIImage* _croppedImage;
    BOOL _isUserProfilePics;
    NSPredicate* _pu;
}

@end

@implementation ProfileFeedViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
        
    // To do: check friends
    _tmpContext = [NSManagedObjectContext MR_contextWithParent:[NSManagedObjectContext MR_contextForCurrentThread]];
    

    _isUserProfilePics = YES;
    
    if(self.public) {
        _upublic = [User_Public MR_importFromObject:self.public inContext:_tmpContext];
    } else {
        _upublic = [User_Public MR_createInContext:_tmpContext];
        _upublic.id = [Helper sharedHelper].currentUser.id;
        _upublic.username = [Helper sharedHelper].currentUser.username;
        _upublic.profile_picture = [[Helper sharedHelper].currentUser.profile objectForKey:@"picture"];
    }
    
    _type = ([_upublic.id isEqual:[Helper sharedHelper].currentUser.id]) ? own : friend;
    
    self.title = _upublic.username;
    
    if([_upublic.username isEqualToString:[Helper sharedHelper].currentUser.username]) {
        UIBarButtonItem* settings = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"profileSettingsIcon"] style:UIBarButtonItemStylePlain target:self action:@selector(settingsButtonPressed)];
        
        self.navigationItem.rightBarButtonItem = settings;
    }
    
    _p = [NSPredicate predicateWithFormat:@"author = %@", _upublic];
    
    UINib* nib = [UINib nibWithNibName:@"PostPublicTableViewCell" bundle:nil];
    
    [self.userPostsTableView registerNib:nib forCellReuseIdentifier:kFeedCellReuseIdentifier];
    
    _profileHeaderView = (ProfileTableHeaderView*)[[[NSBundle mainBundle] loadNibNamed:@"ProfileTableHeaderView" owner:self options:nil] objectAtIndex:0];
    
    _profileHeaderView.delegate = self;
    
    self.userPostsTableView.tableHeaderView = _profileHeaderView;
    
    [self fetchProfile];
    
    _blurView = [[ILTranslucentView alloc] initWithFrame:self.view.bounds];
    _blurView.translucentAlpha = 0;
    _blurView.translucentStyle = UIBarStyleBlackTranslucent;
    _blurView.translucentTintColor = [UIColor clearColor];
    _blurView.backgroundColor = [UIColor clearColor];
    
    _imgPicker = [[UIImagePickerController alloc] init];
    _imgPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    _imgPicker.delegate = self;
    _imgPicker.allowsEditing = YES;
    _imgPicker.navigationBar.translucent = NO;
    
    _profileHeaderView.actionButton.hidden = YES;
    
    _pu = [NSPredicate predicateWithFormat: @"id == %@", [Helper sharedHelper].currentUser.id];
}

-(void)setUpButton {
    
    [_profileHeaderView.actionButton removeTarget:self action:@selector(followUser) forControlEvents:UIControlEventTouchUpInside];
    [_profileHeaderView.actionButton removeTarget:self action:@selector(unfollowUser) forControlEvents:UIControlEventTouchUpInside];
    
    NSArray* user = [[_profile.followers allObjects] filteredArrayUsingPredicate:_pu];
    
    if(user.count > 0){
        // follower
        [_profileHeaderView.actionButton setTitle:@"Unfollow" forState:UIControlStateNormal];
        [_profileHeaderView.actionButton addTarget:self action:@selector(unfollowUser) forControlEvents:UIControlEventTouchUpInside];
        _profileHeaderView.actionButton.hidden = NO;
    } else if([_profile.id isEqual:[Helper sharedHelper].currentUser.id]) {
        [_profileHeaderView.actionButton setTitle:@"Profile settings" forState:UIControlStateNormal];
        [_profileHeaderView.actionButton addTarget:self action:@selector(profileSettings) forControlEvents:UIControlEventTouchUpInside];
        _profileHeaderView.actionButton.hidden = NO;
    } else {
        [_profileHeaderView.actionButton setTitle:@"Follow" forState:UIControlStateNormal];
        [_profileHeaderView.actionButton addTarget:self action:@selector(followUser) forControlEvents:UIControlEventTouchUpInside];
        _profileHeaderView.actionButton.hidden = NO;
    }
    
    [_profileHeaderView.actionButton sizeToFit];
}

-(void)profileSettings {
    [self performSegueWithIdentifier:@"ToProfileSettingsFromProfile" sender:nil];
}

-(void)followUser {
    NSString* followEndpoint = [[@"users/" stringByAppendingString:_profile.id.description] stringByAppendingString:@"/followers"];
    
    [[APIAgent sharedAgent] postToEndpoint: followEndpoint withParams:@{} onSuccess:^(id responseObject) {
        //code
        
        NSMutableSet* tmp = [NSMutableSet setWithSet:_profile.followers];

        [tmp addObject:[User_Public MR_importFromObject:responseObject inContext:_tmpContext]];
        
        _profile.followers = [NSSet setWithSet:tmp];
        
        [self setUpButton];

    } onFail:^(id error) {
        //code
    }];
}

-(void)unfollowUser {
    NSString* followEndpoint = [[@"users/" stringByAppendingString:_profile.id.description] stringByAppendingString:@"/followers"];
    
    [[APIAgent sharedAgent] deleteFromEndpoint: followEndpoint withParams:@{} onSuccess:^(id responseObject) {
        //code
        
        NSArray* user = [[_profile.followers allObjects] filteredArrayUsingPredicate:_pu];
        
        NSMutableSet* followers = [NSMutableSet setWithSet:_profile.followers];
        
        [followers removeObject:user[0]];
        
        _profile.followers = [NSSet setWithSet:followers];
        
        [self setUpButton];
        
    } onFail:^(id error) {
        //code
    }];
}

// patch
- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

-(void)settingsButtonPressed {
    NSLog(@"Settings button pressed");
    [self performSegueWithIdentifier:@"ToSettingsFromProfile" sender:nil];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self setUpRecognizer];
    [self checkTempPost];
}

#pragma mark Tableview gesture recognizer

-(void)setUpRecognizer {
    UISwipeGestureRecognizer *r = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(didSwipe:)];
    [r setDirection:UISwipeGestureRecognizerDirectionRight];
    [self.userPostsTableView addGestureRecognizer:r];
    r = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(didSwipe:)];
    [r setDirection:UISwipeGestureRecognizerDirectionLeft];
    [self.userPostsTableView addGestureRecognizer:r];

}

- (void)didSwipe:(UIGestureRecognizer *)gestureRecognizer {
    CGPoint swipeLocation = [gestureRecognizer locationInView:self.userPostsTableView];
    NSIndexPath *swipedIndexPath = [self.userPostsTableView indexPathForRowAtPoint:swipeLocation];
    PostPublicTableViewCell* swipedCell = (PostPublicTableViewCell*)[self.userPostsTableView cellForRowAtIndexPath:swipedIndexPath];
    [swipedCell performSwipe:gestureRecognizer];
    NSLog(@"triggered swipe on %@", swipedCell);
    NSLog(@"%@", gestureRecognizer);
}

#pragma mark Data

- (void) fetchPosts {
    NSString* endpoint = [[@"users/" stringByAppendingString:_upublic.id.description] stringByAppendingString:@"/posts"];
    [[APIAgent sharedAgent] getFromEndpoint:endpoint onSuccess:^(id responseObject) {
        NSLog(@"DATA: %@", responseObject);
        
        if(![responseObject isEqual:[NSNull null]]) {

            [responseObject enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                Post_Public* post =[Post_Public MR_importFromObject:obj inContext:_tmpContext];
                post.author = _upublic;
            }];
        
            _posts = [Post_Public MR_findAllSortedBy:@"created_time" ascending:NO withPredicate:_p inContext:_tmpContext];
        
            [self.userPostsTableView reloadData];
            
        }
        
    } onFail:^(NSError *error) {
        NSLog(@"%@",error);
    }];

}

- (void) fetchProfile {
    NSString* endpoint = [@"users/" stringByAppendingString:_upublic.id.description];
    [[APIAgent sharedAgent] getFromEndpoint:endpoint onSuccess:^(NSDictionary* responseObject) {
        NSLog(@"DATA: %@", responseObject);
        
        _profile = [User_Profile MR_importFromObject:responseObject inContext:_tmpContext];
        _profile.user_public = _upublic;
        
        [_profileHeaderView configureView:_profile];
        
        NSLog(@"%@", _profile.private);
        
        if([_profile.private isEqualToNumber:[NSNumber numberWithInt:0]]
           || _type == own) {
            [self fetchPosts];
        }
        
        if(_type != own) {
            [_profileHeaderView setEditable:NO];
        }
        
        [self setUpButton];
        
    } onFail:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}


-(void)checkTempPost {
    if(_tmpPost != nil) {
        NSString* postEndpoint = [@"posts/" stringByAppendingString:_tmpPost.id.description];
        [[APIAgent sharedAgent] getFromEndpoint:postEndpoint onSuccess:^(id responseObject) {
            NSDictionary* r = (NSDictionary*)responseObject;
            NSDictionary* cl = [r objectForKey:@"comments"];
            
            Comment_Preview* cp = [Comment_Preview MR_createInContext:_tmpContext];
            
            NSArray* comments = [cl objectForKey:@"data"];
            
            cp.count = [cl objectForKey:@"count"];
            cp.data = [NSSet new];
            
            NSMutableSet* tSet = [[NSMutableSet alloc] init];
            if(![comments isEqual:[NSNull null]]) {

            [comments enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                NSDictionary* current = (NSDictionary*)obj;
                NSDictionary* from = [current objectForKey:@"from"];
                
                Comment_Public* cl = [Comment_Public MR_createInContext:_tmpContext];
                cl.id = [current objectForKey:@"id"];
                cl.message = [current valueForKey:@"message"];
                
                cl.created_time = [current objectForKey:@"created_time"];
                
                User_Public *p = [User_Public MR_createInContext:_tmpContext];
                p.id = [from objectForKey:@"id"];
                p.username = [from valueForKey:@"username"];
                p.profile_picture = NILIFNULL([from objectForKey:@"profile_picture"]);
                
                cl.from = p;
                
                [tSet addObject:cl];
            }];

            cp.data = [NSSet setWithSet:tSet];

            }
            
            NSDictionary* coolz = [responseObject objectForKey:@"coolerz"];
            
            Cool_Preview* cool = [Cool_Preview MR_createInContext:_tmpContext];
            
            cool.count = [coolz objectForKey:@"count"];
            
            NSArray* coolerz = [coolz objectForKey:@"data"];
            
            tSet = [[NSMutableSet alloc] init];
            
            if(![coolerz isEqual:[NSNull null]]) {
                
                
                [coolerz enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    [tSet addObject:[User_Public MR_importFromObject:obj inContext:_tmpContext]];
                }];
                
                cool.data = [NSSet setWithSet:tSet];
            }
            
            _tmpPost.comments = cp;
            _tmpPost.coolerz = cool;
            
            [self.userPostsTableView reloadRowsAtIndexPaths:@[_tmpIndexPath] withRowAnimation:UITableViewRowAnimationNone];
            
        } onFail:^(id error) {
            
        }];
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _posts.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PostPublicTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kFeedCellReuseIdentifier];
    
    [cell forceResetCell];
    
    NSInteger idx = indexPath.row;
    
    Post_Public *row = _posts[idx];
    
    cell.post = row;
    
    [cell configureCellforType:_type withIndexPath:indexPath];
    
    cell.delegate = self;
    
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 320;
}

#pragma mark Header delegate

-(void)onFollowersLabelTouched {
    [self performSegueWithIdentifier:@"ToRelationFromProfile" sender:@{@"type":@"no"}];
}

-(void)onFollowingLabelTouched {
    [self performSegueWithIdentifier:@"ToRelationFromProfile" sender:@{@"type":@"yes"}];
}

-(void)onProfilePictureTouched {
    _isUserProfilePics = YES;
    [self presentViewController:_imgPicker animated:YES completion:NULL];
}

-(void)onCoverPictureTouched {
    _isUserProfilePics = NO;
    [self presentViewController:_imgPicker animated:YES completion:NULL];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    if (info) {
        UIImage* outputImage = [info objectForKey:UIImagePickerControllerEditedImage];
        if (outputImage == nil) {
            outputImage = [info objectForKey:UIImagePickerControllerOriginalImage];
        }
        
        if (outputImage) {
            _croppedImage = outputImage;
            [picker dismissViewControllerAnimated:YES completion:nil];
            
            if(_isUserProfilePics) {
            
                [[APIAgent sharedAgent] postToEndpoint:@"users/me" withParams:@{} withImage:    _croppedImage andName:@"picture" onSuccess:^(NSDictionary* responseObject) {
                
                    _profile.profile = [responseObject objectForKey:@"profile"];
                    [_profileHeaderView configureView:_profile];
                
                    NSLog(@"%@", responseObject);
                } onFail:^(NSError *error) {
                
                }];
            } else {
                [[APIAgent sharedAgent] postToEndpoint:@"users/me" withParams:@{} withImage:    _croppedImage andName:@"cover" onSuccess:^(NSDictionary* responseObject) {
                    
                    _profile.profile = [responseObject objectForKey:@"profile"];
                    [_profileHeaderView configureView:_profile];
                    
                    NSLog(@"%@", responseObject);
                } onFail:^(NSError *error) {
                    
                }];
            }
        }
    }
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:^{
        
    }];
}

#pragma mark Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // ToRelationFromProfile
    
    if([segue.identifier isEqual:@"ToCommentFromProfile"]) {
        CommentViewController* c = (CommentViewController*)segue.destinationViewController;
        c.post = _tmpPost;
    } else if([segue.identifier isEqual:@"ToCoolersFromProfile"]) {
        CoolersViewController* c = (CoolersViewController*)segue.destinationViewController;
        c.preview = _tmpPost.coolerz;
        c.postId = _tmpPost.id;
    } else if([segue.identifier isEqual:@"ToProfileFeedView"]) {
        ProfileFeedViewController* c = (ProfileFeedViewController*)segue.destinationViewController;
        if(!sender) {
            c.public = _tmpPost.author;
        } else if([sender isKindOfClass:[Comment_Public class]]) {
            Comment_Public* co = (Comment_Public*)sender;
            c.public = co.from;
        }
    } else if([segue.identifier isEqual:@"ToRelationFromProfile"]) {
        RelationViewController* c = (RelationViewController*)segue.destinationViewController;
        c.userId = _profile.id;
        if([[sender valueForKey:@"type"] isEqualToString:@"yes"]) {
            c.type = YES;
        } else {
            c.type = NO;
        }
    } else if([segue.identifier isEqual:@"ToProfileSettingsFromProfile"]) {
        ProfileSettingsViewController* c = (ProfileSettingsViewController*)segue.destinationViewController;
        c.profile = _profile;
        c.inheritedContext = _tmpContext;
    }
}

#pragma mark delegate methods

-(void)onCommentButtonPress:(Post_Public*)post
                 withSender:(PostPublicTableViewCell*)cell
               andIndexPath:(NSIndexPath*)path
{
    _tmpIndexPath = path;
    _tmpPost = post;
    [cell resetCell];
    [self performSegueWithIdentifier:@"ToCommentFromProfile" sender:nil];
}

-(void)onProfileAction:(Post_Public*)post
        withActionType:(ProfileActionType)type
             andSender:(PostPublicTableViewCell*)cell
          andIndexPath:(NSIndexPath*)path
{
    switch (type) {

        case own: {
            //deletePost
            _tmpPost = post;
            _tmpIndexPath = path;
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle:@"Confirm!"
                                  message:@"Are you sure you want to delete this post?"
                                  delegate: self
                                  cancelButtonTitle:@"Cancel"
                                  otherButtonTitles:@"OK", nil];
            [alert show];
            break;
        }
                default:
            break;
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if(buttonIndex == 1)
    {
        NSString* deletePostEndpoint = [@"posts/" stringByAppendingString:_tmpPost.id.description];
        [[APIAgent sharedAgent] deleteFromEndpoint: deletePostEndpoint withParams:@{} onSuccess:^(id responseObject) {
            //code
            [_tmpPost MR_deleteEntity];
            [self fetchPosts];
            [self.userPostsTableView reloadData];
        } onFail:^(id error) {
            //code
        }];
    }
    
}

-(void)onCommentsLabelTouched:(Post_Public*)post
                   withSender:(PostPublicTableViewCell*)cell
                 andIndexPath:(NSIndexPath*)path
{
    
}

-(void)onCoolersLabelTouched:(Post_Public*)post
                  withSender:(PostPublicTableViewCell*)cell
                andIndexPath:(NSIndexPath*)path
{
    _tmpIndexPath = path;
    _tmpPost = post;
    [cell resetCell];
    [self performSegueWithIdentifier:@"ToCoolersFromProfile" sender:nil];
}

-(void)onUsernameLabelTouched:(Post_Public*)post
                   withSender:(PostPublicTableViewCell*)cell
                 andIndexPath:(NSIndexPath*)path {
    NSLog(@"username label tapped.");
    
}

-(void)onDoubleTap:(Post_Public *)post withSender:(PostPublicTableViewCell *)cell andIndexPath:(NSIndexPath *)path {
    
    _tmpPost = post;
    _tmpIndexPath = path;
    
    NSString* postEndpoint = [[@"posts/" stringByAppendingString:post.id.description] stringByAppendingString:@"/coolers"];
    
    [[APIAgent sharedAgent] postToEndpoint:postEndpoint withParams:@{} onSuccess:^(id responseObject) {
        [self checkTempPost];

    } onFail:^(id error) {
        NSLog(@"%@", error);
    }];
    
}

-(void)onCommentUsernameLabelTouched:(Comment_Public *)comment withPost:(Post_Public *)post withSender:(PostPublicTableViewCell *)cell andIndexPath:(NSIndexPath *)path {
    _tmpIndexPath = path;
    _tmpPost = post;
    [self performSegueWithIdentifier:@"ToProfileFeedView" sender:comment];
}

#pragma mark header delegate

-(void)onAboutTextChanged:(NSString*)about {
    [[APIAgent sharedAgent] putOnEndpoint:@"users/me" withParams:@{@"profile": @{@"about": about}} onSuccess:^(id responseObject) {
        //code
    } onFail:^(id error) {
        //code
    }];
}


@end
