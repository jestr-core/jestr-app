//
//  MenuButton.m
//  Jestr
//
//  Created by Árpád Kiss on 2014.05.19..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import "MenuButton.h"
#import "AVHexColor.h"
#import "Helper.h"

@implementation MenuButton {
    UIImage *_img;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {

    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    if(_img == nil) {
        _img = [self imageForState:UIControlStateNormal];
        [self setImage:nil forState:UIControlStateNormal];
    }
    self.tintColor = [UIColor whiteColor];
    self.titleLabel.font = [UIFont systemFontOfSize:22];
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [Helper sharedHelper].secondaryColor.CGColor);
    CGContextFillRect(context, CGRectMake(0.0f, 0.0f, self.frame.size.width, 1.0));
    
    CGContextSetFillColorWithColor(context, [Helper sharedHelper].menuButtonColor.CGColor);
    CGContextFillRect(context, CGRectMake(0.0f, 1.0f, self.frame.size.width, self.frame.size.height-1.0));
    
    CGContextSetFillColorWithColor(context, [Helper sharedHelper].secondaryColor.CGColor);
    CGContextTranslateCTM(context, self.imageView.frame.origin.x, self.imageView.image.size.height / 2 + rect.size.height / 2);
    CGContextScaleCTM(context, 1.0, -1.0);
    CGContextClipToMask(context, self.imageView.bounds, [_img CGImage]);
    CGContextFillRect(context, self.imageView.bounds);
}

- (CGRect)imageRectForContentRect:(CGRect)contentRect
{
    CGRect frame = [super imageRectForContentRect:contentRect];
    frame.origin.x = 30;
    return frame;
}

@end
