//
//  Helper.m
//  Jestr
//
//  Created by Árpád Kiss on 2014.04.28..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import "Helper.h"
#import "APIAgent.h"
#import "AVHexColor/AVHexColor.h"

static NSString* const kSettingsFile = @"jestSettings.plist";

@implementation Helper

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.currentUser = [self loadCurrentUser];
        self.mainColor = [AVHexColor colorWithHex:0xE81D60];
        self.secondaryColor = [AVHexColor colorWithHex:0x28BBB9];
        //self.mainColor = [AVHexColor colorWithHex:0xAFFF7A];
        //self.secondaryColor = [AVHexColor colorWithHex:0xCA7AFF];
        self.menuButtonColor = [AVHexColor colorWithHex:0x218281];
    }
    return self;
}

+(Helper*) sharedHelper {
    static id _sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[self alloc] init];
    });
    return _sharedInstance;
}

- (CurrentUser *)loadCurrentUser
{
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:@"currentUser"];
    if(data)
    {
        NSArray *cu = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        return [cu firstObject];
    }
    return nil;
}

//-(User_Public*)publicUser {
//    NSManagedObjectContext* c = [NSManagedObjectContext MR_contextForCurrentThread];
//    NSPredicate* p = [NSPredicate predicateWithFormat:@"id == %@", self.currentUser.id];
//    NSArray* u = [User_Public MR_findAllWithPredicate:p inContext:c];
//    
//    NSLog(@"%@", u);
//    
//    return (u.count == 1) ? u[0] : nil;
//}


- (void)saveCurrentUser:(CurrentUser *)currentUser {

    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:@[currentUser]];
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"currentUser"];
    
    self.currentUser = currentUser;
}

-(void)logoutCurrentUser {
    self.currentUser = nil;
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"currentUser"];
    [[APIAgent sharedAgent] resetSession];
}

- (NSString *)documentDirectory
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];
    
    NSLog(@"documentDirectory: %@",documentDirectory);
    
    return documentDirectory;
}

-(void)setNavbarStyle {
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    [[UINavigationBar appearance] setBarTintColor:self.mainColor];
    
    NSShadow *shadow = [[NSShadow alloc] init];
    shadow.shadowColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.2];
    shadow.shadowOffset = CGSizeMake(1, 1);
    
    [[UINavigationBar appearance] setTitleTextAttributes:
     @{NSForegroundColorAttributeName : [UIColor whiteColor], NSShadowAttributeName : shadow}];
    
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60)
                                                         forBarMetrics:UIBarMetricsDefault];
}

- (BOOL)validateEmail: (NSString *) candidate {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:candidate];
}

-(void)registerDeviceForNotifs {
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes: (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)
    ];
}

-(NSString *)dateDiff:(double)origDate {
    NSDate *convertedDate = [NSDate dateWithTimeIntervalSince1970:origDate];
    NSDate *todayDate = [NSDate date];
    double ti = [convertedDate timeIntervalSinceDate:todayDate];
    ti = ti * -1;
    if(ti < 1) {
    	return @"now";
    } else 	if (ti < 60) {
    	return @"<1m";
    } else if (ti < 3600) {
    	int diff = round(ti / 60);
    	return [NSString stringWithFormat:@"%dm", diff];
    } else if (ti < 86400) {
    	int diff = round(ti / 60 / 60);
    	return[NSString stringWithFormat:@"%dh", diff];
    } else if (ti < 604800) {
    	int diff = round(ti / 60 / 60 / 24);
    	return[NSString stringWithFormat:@"%dd", diff];
    } else {
        int diff = round(ti / 60 / 60 / 24 / 7);
        return[NSString stringWithFormat:@"%dw", diff];
    }
}

@end
