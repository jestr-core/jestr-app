//
//  CoolerTableViewCell.h
//  Jestr
//
//  Created by Árpád Kiss on 2014.07.01..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User_Public.h"

@interface CoolerTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;

-(void)configureCell:(User_Public*)user;

@end
