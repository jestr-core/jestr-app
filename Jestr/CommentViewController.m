//
//  CommentViewController.m
//  Jestr
//
//  Created by Árpád Kiss on 2014.06.08..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import "StorageManager.h"
#import "CommentViewController.h"
#import "Comment_Public.h"
#import "Comment_Preview.h"
#import "User_Public.h"
#import "Helper.h"
#import "AVHexColor.h"
#import "CommentPublicTableViewCell.h"
#import "APIAgent.h"
#import "UIImageView+AFNetworking.h"
#import "ILTranslucentView.h"
#import "Helper.h"
#import "ProfileFeedViewController.h"

@interface CommentViewController () {
    NSArray* _comments;
    NSManagedObjectContext* _tmpContext;
    NSString* _commentEndpoint;
    NSPredicate* _p;
    Comment_Preview* _comment_preview;
}

@end

@implementation CommentViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _commentEndpoint = [[@"posts/" stringByAppendingString:self.post.id.description] stringByAppendingString:@"/comments"];
    // Do any additional setup after loading the view.
    
    _tmpContext = [NSManagedObjectContext MR_contextWithParent:[NSManagedObjectContext MR_contextForCurrentThread]];
    
    
    NSLog(@"%@", self.post);
    
    //_comment_preview = [self.post.comments MR_inContext:_tmpContext];
    _comment_preview = [Comment_Preview MR_createInContext:_tmpContext];
    
    _p = [NSPredicate predicateWithFormat:@"comment_preview = %@", _comment_preview];
    
    [self configureView];
    [self fetchComments];
    [self.commentInputTextField becomeFirstResponder];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.commentInputTextField becomeFirstResponder];
}

-(void)configureView {
    NSString *imageURL = [@"http://api.jestr.me/"
                          stringByAppendingString:EMPTYIFNIL(
                                                             [self.post.images valueForKey:@"standard"])];
    [self.postBackgroundView setImageWithURL:[NSURL URLWithString:imageURL]];
    ILTranslucentView* blurView = [[ILTranslucentView alloc] initWithFrame:self.postBackgroundView.bounds];
    blurView.translucentAlpha = 1;
    blurView.translucentStyle = UIBarStyleBlackTranslucent;
    blurView.translucentTintColor = [UIColor clearColor];
    blurView.backgroundColor = [UIColor clearColor];
    [self.postBackgroundView addSubview:blurView];
    self.postActionButton.layer.cornerRadius = 4;
    self.commentInputTextField.layer.cornerRadius = 4;
    self.view.backgroundColor = [AVHexColor colorWithHex:0x28BBB9];
    self.commentInputContainer.backgroundColor = [AVHexColor colorWithHex:0x28BBB9];
    [self.postActionButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.postActionButton.backgroundColor = [AVHexColor colorWithHex:0xE81D60];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// row patch
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Comment_Public* c = (Comment_Public*)_comments[indexPath.row];
    
    CGRect r = [c.message boundingRectWithSize:CGSizeMake(236, 0)
                                                        options:NSStringDrawingUsesLineFragmentOrigin
                                                     attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15]}
                                                        context:nil];

    return 74 + r.size.height - 21;
}

-(BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}


#pragma mark REFACTOR
-(void)fetchComments
{
    [[APIAgent sharedAgent] getFromEndpoint:_commentEndpoint onSuccess:^(id responseObject) {
        NSLog(@"DATA: %@", responseObject);
        
        if(NILIFNULL(responseObject) == nil){
            _comments = [[NSArray alloc] init];
            return;
        }
        
        [responseObject enumerateObjectsUsingBlock:^(NSDictionary* obj, NSUInteger idx, BOOL *stop) {

            Comment_Public* comment = [Comment_Public MR_createInContext:_tmpContext];
            comment.id = [obj objectForKey:@"id"];
            comment.message = [obj valueForKey:@"message"];
            comment.created_time = [obj objectForKey:@"created_time"];
            comment.comment_preview = _comment_preview;
            
            NSDictionary* from = [obj objectForKey:@"from"];
            
            User_Public *p = [User_Public MR_createInContext:_tmpContext];
            p.id = [from valueForKey:@"id"];
            p.username = [from valueForKey:@"username"];
            p.profile_picture = NILIFNULL([from objectForKey:@"profile_picture"]);
            
            comment.from = p;
            
        }];

        _comments = [Comment_Public MR_findAllSortedBy:@"created_time" ascending:YES withPredicate:_p inContext:_tmpContext];
        
        [self.commentTableView reloadData];
        
    } onFail:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _comments.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CommentPublicTableViewCell" owner:self options:nil];
    
    CommentPublicTableViewCell *cell = (CommentPublicTableViewCell*)[nib objectAtIndex:0];
    
    NSInteger idx = indexPath.row;
    
    Comment_Public *c = (Comment_Public*)_comments[idx];
    
    [cell configureCell:c];
    
    cell.delegate = self;
    
    return cell;
}

-(void)onUsernameLabelTapped:(Comment_Public*)comment {
    [self performSegueWithIdentifier:@"ToProfileFromComment" sender:comment];
}

- (IBAction)onPostButtonPress:(id)sender {
    
    NSString* message = [self.commentInputTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if(message.length == 0) {
        return;
    }
    
    NSDictionary* data = @{
                           @"message": message
                           };
    [[APIAgent sharedAgent] postToEndpoint:_commentEndpoint withParams:data onSuccess:^(id responseObject) {
        
        NSDictionary* r = (NSDictionary*)responseObject[0];
        
        NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
        [f setNumberStyle:NSNumberFormatterDecimalStyle];
        
        Comment_Public* c = [Comment_Public MR_createInContext:_tmpContext];
        c.id = [f numberFromString:[r valueForKey:@"id"]];
        c.message = [r valueForKey:@"message"];
        c.created_time = [f numberFromString:[r valueForKey:@"date"]];
        c.comment_preview = _comment_preview;
        
        User_Public *p = [User_Public MR_createInContext:_tmpContext];
        p.id = [Helper sharedHelper].currentUser.id;
        p.username = [Helper sharedHelper].currentUser.username;
        p.profile_picture = [[Helper sharedHelper].currentUser.profile objectForKey:@"picture"];
        
        c.from = p;
        
        _comments = [Comment_Public MR_findAllSortedBy:@"created_time" ascending:YES withPredicate:_p inContext:_tmpContext];
        
        self.commentInputTextField.text = @"";
        
        [self.commentTableView reloadData];
        [self.commentTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:_comments.count-1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
        
    } onFail:^(id error) {
        //code
    }];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"ToProfileFromComment"]) {
        ProfileFeedViewController* vc = (ProfileFeedViewController*)segue.destinationViewController;
        Comment_Public* cp = (Comment_Public*)sender;
        vc.public = cp.from;
    }
}


@end
