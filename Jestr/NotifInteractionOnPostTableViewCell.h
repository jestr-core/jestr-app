//
//  NotifInteractionOnPostTableViewCell.h
//  Jestr
//
//  Created by Árpád Kiss on 2014.07.03..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Notification.h"

@interface NotifInteractionOnPostTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UIImageView *thumbnailImageView;


-(void)configureCell:(Notification*)notif withMessage:(NSString*)message;

@end
