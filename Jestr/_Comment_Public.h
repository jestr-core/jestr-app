//
//  Comment_Public.h
//  Jestr
//
//  Created by Árpád Kiss on 2014.05.17..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class User_Public;

@interface Comment_Public : NSManagedObject

@property (nonatomic, retain) NSNumber * id;
@property (nonatomic, retain) NSNumber * created_time;
@property (nonatomic, retain) NSString * message;
@property (nonatomic, retain) User_Public *from;

@end
