//
//  Helper.h
//  Jestr
//
//  Created by Árpád Kiss on 2014.04.28..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CurrentUser.h"
#import "User_Public.h"

@interface Helper : NSObject

@property (nonatomic, strong) CurrentUser *currentUser;
@property (nonatomic, strong) NSString* userAPNSToken;

#pragma mark Editor cache
@property (nonatomic, strong) UIImage* editorBackground;
@property (nonatomic, strong) UIImage* editorLayer;
@property (nonatomic, strong) UIImage* editorThumbnail;

#pragma mark Colors
@property (nonatomic, strong) UIColor* mainColor;
@property (nonatomic, strong) UIColor* secondaryColor;
@property (nonatomic, strong) UIColor* menuButtonColor;

+(Helper*) sharedHelper;

-(void)saveCurrentUser:(CurrentUser *)currentUser;
-(void)logoutCurrentUser;


-(void)setNavbarStyle;

-(BOOL)validateEmail:(NSString *)candidate;

-(void)registerDeviceForNotifs;

-(NSString *)dateDiff:(double)origDate;

@end
