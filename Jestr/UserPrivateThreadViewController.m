//
//  UserPrivateThreadViewController.m
//  Jestr
//
//  Created by Árpád Kiss on 2014.07.02..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import "UserPrivateThreadViewController.h"
#import "Post_Message.h"
#import "APIAgent.h"
#import "PostMessageTableViewCell.h"
#import "Helper.h"

@interface UserPrivateThreadViewController () {
    NSArray* _messages;
    NSManagedObjectContext* _tmpContext;
    Post_Message* _tmpMsg;
    UIRefreshControl* _r;
    int _page;
}

@end

@implementation UserPrivateThreadViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = self.friend.username;
    
    _tmpContext = [NSManagedObjectContext MR_contextWithParent:[NSManagedObjectContext MR_contextForCurrentThread]];
    UINib* nib = [UINib nibWithNibName:@"PostMessageTableViewCell" bundle:nil];
    
    [self.messagesTableView registerNib:nib forCellReuseIdentifier:kPostMessageCellReuseIdentifier];
    
    [self fetchMessages];

    _r = [[UIRefreshControl alloc] init];
    _r.tintColor = [Helper sharedHelper].secondaryColor;
    [_r addTarget:self action:@selector(refreshMessages) forControlEvents:UIControlEventValueChanged];
    
    [self.messagesTableView addSubview:_r];
}
-(void)refreshMessages {
    _page = 0;
    [self fetchMessages];
}

-(void)fetchMessages {
    NSString* endpoint = [[@"users/me/messages/" stringByAppendingString:self.friend.id.description] stringByAppendingFormat:@"?page=%i", _page];
    [[APIAgent sharedAgent] getFromEndpoint:endpoint onSuccess:^(id responseObject) {
        NSLog(@"DATA: %@", responseObject);
        
        if(![responseObject isEqual:[NSNull null]]) {
        
        [responseObject enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            [Post_Message MR_importFromObject:obj inContext:_tmpContext];
        }];
        
        _messages = [Post_Message MR_findAllSortedBy:@"created_time" ascending:NO inContext:_tmpContext];
        
        [self.messagesTableView reloadData];
        
        [_r endRefreshing];
        
        } else {
            [_r endRefreshing];
            _page = (_page == 0) ? 0 : _page - 1;
        }
        
    } onFail:^(NSError *error) {
        NSLog(@"%@",error);
    }];

}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row == _messages.count - 1) {
        _page = _page + 1;
        [self fetchMessages];
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _messages.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PostMessageTableViewCell* cell = [self.messagesTableView dequeueReusableCellWithIdentifier:kPostMessageCellReuseIdentifier];
    [cell forceResetCell];
    [cell configureCell:_messages[indexPath.row]];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 320;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        _tmpMsg = _messages[indexPath.row];
        //add code here for when you hit delete
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@"Confirm!"
                              message:@"Are you sure you want to delete this post?"
                              delegate: self
                              cancelButtonTitle:@"Cancel"
                              otherButtonTitles:@"OK", nil];
        [alert show];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if(buttonIndex == 1)
    {
        [_tmpMsg MR_deleteEntity];
        _messages = [Post_Message MR_findAllSortedBy:@"created_time" ascending:NO inContext:_tmpContext];
        [self.messagesTableView reloadData];
    }
    
}


//-(BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath{
//    return NO;
//}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
