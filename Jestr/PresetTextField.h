//
//  PresetTextField.h
//  Jestr
//
//  Created by Árpád Kiss on 2014.07.08..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreText/CoreText.h>

@interface PresetTextField : UITextField

@property (nonatomic) CTFontRef fontForRendering;
@property (nonatomic) CGFloat originalFontSize;
@property (nonatomic) CGPoint position;
@property (nonatomic) CGAffineTransform referenceTransform;
@property (nonatomic) CGRect origFrame;
@property (nonatomic) NSString* label;
@property (nonatomic) CGPoint startPoint;

@end
