//
//  PresetTableViewCell.m
//  Jestr
//
//  Created by Árpád Kiss on 2014.05.21..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import "PostPublicTableViewCell.h"
#import "Comment_Preview.h"
#import "Cool_Preview.h"
#import "User_Public.h"
#import "UIImageView+AFNetworking.h"

@implementation PostPublicTableViewCell {
    NSArray *_comments;
}

- (void)awakeFromNib
{
    self.separatorInset = UIEdgeInsetsMake(0, 0, 0, self.bounds.size.width);
    self.commentPreTableView.delegate = self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setPost:(Post_Public *)post {
    if(!self.post) {
        self.post = post;
        _comments = [self.post.comments.data allObjects];
    }
    [self configureCell];
}

#pragma mark tv delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _comments.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
    
    NSInteger idx = indexPath.row;
    
    cell.textLabel.text = _comments[idx];
    
    return cell;
}

-(void)configureCell {
    [self.commentPreTableView reloadData];
    NSString *imageURL = [@"http://api.jestr.me/"
                          stringByAppendingString:EMPTYIFNIL(                                                       [self.post.images valueForKey:@"standard"])];
    [self.backgroundImageView setImageWithURL:[NSURL URLWithString:imageURL]];
    
    NSString *profileImageURL = [@"http://api.jestr.me/"
                                 stringByAppendingString:EMPTYIFNIL(                                                       [self.post.author.profile_picture valueForKey:@"medium"])];
    
    [self.profileImageView setImageWithURL:[NSURL URLWithString:profileImageURL]];
    self.commentsLabel.text = [self.post.comments.count stringValue];
    self.coolLabel.text = [self.post.coolerz.count stringValue];
    self.dateLabel.text = [self.post.created_time stringValue];
    self.commentPreTableView.bounds = CGRectMake(320, 0, 320, 320);
    self.profilePreviewView.bounds = CGRectMake(-320, 0, 320, 320);
}
- (IBAction)swipeHappened:(id)sender {
    NSLog(@"SWÍÍÍÍPE");
}

@end
