//
//  RelationViewController.m
//  Jestr
//
//  Created by Árpád Kiss on 2014.07.14..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import "RelationViewController.h"
#import "User_Public.h"
#import "Helper.h"
#import "APIAgent.h"
#import "CoolerTableViewCell.h"
#import "ProfileFeedViewController.h"

@interface RelationViewController () {
    NSString* _endpoint;
    NSArray* _users;
    NSMutableSet* _tmp;
    NSManagedObjectContext* _tmpContext;
    User_Public* _selectedUser;
}

@end

@implementation RelationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _tmpContext = [NSManagedObjectContext MR_contextWithParent:[NSManagedObjectContext MR_contextForCurrentThread]];
    
    if(!self.type) {
        self.title = @"Followers";
        _endpoint = [[@"users/" stringByAppendingString:self.userId.description] stringByAppendingString:@"/followers"];
    } else {
        self.title = @"Following";
        _endpoint = [[@"users/" stringByAppendingString:self.userId.description] stringByAppendingString:@"/following"];
    }
    
    UINib* nib = [UINib nibWithNibName:@"CoolerTableViewCell" bundle:nil];
    [self.relationTableView registerNib:nib forCellReuseIdentifier:kCoolerCellReuseIdentifier];
    
    [self fetchUserRelatives];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)fetchUserRelatives {
    
    _tmp = [[NSMutableSet alloc] init];
    
    [[APIAgent sharedAgent] getFromEndpoint:_endpoint onSuccess:^(id responseObject) {
        NSLog(@"DATA: %@", responseObject);
        
        if(![responseObject isEqual:[NSNull null]]) {
            
            [responseObject enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                [_tmp addObject:[User_Public MR_importFromObject:obj inContext:_tmpContext]];
            }];
            
            _users = [_tmp allObjects];
            [self.relationTableView reloadData];
        }
        
    } onFail:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _users.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CoolerTableViewCell* cell = [self.relationTableView dequeueReusableCellWithIdentifier:kCoolerCellReuseIdentifier];
    [cell configureCell:_users[indexPath.row]];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

-(void)tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    _selectedUser = _users[indexPath.row];
    [self performSegueWithIdentifier:@"ToProfileFromRelation" sender:nil];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if([segue.identifier isEqualToString:@"ToProfileFromRelation"]) {
        ProfileFeedViewController* vc = (ProfileFeedViewController*)segue.destinationViewController;
        vc.public = _selectedUser;
    }
}


@end
