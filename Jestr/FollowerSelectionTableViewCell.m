//
//  FollowerSelectionTableViewCell.m
//  Jestr
//
//  Created by Árpád Kiss on 2014.07.11..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import "FollowerSelectionTableViewCell.h"
#import "UIImageView+AFNetworking.h"

@implementation FollowerSelectionTableViewCell {
    User_Public* _user;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configureCell:(User_Public *)user {
    _user = user;
    self.usernameLabel.text = user.username;
    NSString *profileImageURL = [@"http://api.jestr.me/"
                                 stringByAppendingString:EMPTYIFNIL(                                                       [user.profile_picture valueForKey:@"medium"])];
    [self.profileImageView setImageWithURL:[NSURL URLWithString:profileImageURL]];
}

- (IBAction)followerSelected:(UISwitch*)sender {
    if(sender.on) {
        if([self.delegate respondsToSelector:@selector(followerSelected:)]) {
            [self.delegate followerSelected:_user];
        }
    } else {
        if([self.delegate respondsToSelector:@selector(followerDeselected:)])
        {
            [self.delegate followerDeselected:_user];
        }
    }
}

-(void)drawRect:(CGRect)rect {
    self.profileImageView.layer.cornerRadius = self.profileImageView.bounds.size.width / 2;
    self.profileImageView.layer.masksToBounds = YES;
    self.profileImageView.layer.borderWidth = 1;
    self.profileImageView.layer.borderColor = [[UIColor whiteColor] CGColor];
}

@end
