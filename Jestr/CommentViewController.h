//
//  CommentViewController.h
//  Jestr
//
//  Created by Árpád Kiss on 2014.06.08..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Post_Public.h"
#import "CommentPublicTableViewCell.h"

@interface CommentViewController : UIViewController <
UITableViewDelegate,
UITableViewDataSource,
CommentPublicTableViewCellDelegate
>

@property(nonatomic, strong) Post_Public* post;
@property (weak, nonatomic) IBOutlet UIView *commentInputContainer;
@property (weak, nonatomic) IBOutlet UIButton *postActionButton;
@property (weak, nonatomic) IBOutlet UITextField *commentInputTextField;
@property (weak, nonatomic) IBOutlet UITableView *commentTableView;
@property (weak, nonatomic) IBOutlet UIImageView *postBackgroundView;

@end
