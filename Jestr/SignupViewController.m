//
//  SignupViewController.m
//  Jestr
//
//  Created by Árpád Kiss on 2014.04.26..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import "SignupViewController.h"
#import "APIAgent.h"
#import "Helper.h"

@interface SignupViewController () <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *userNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UILabel *errorLabel;

@end

@implementation SignupViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.userNameTextField.delegate = self;
    self.passwordTextField.delegate = self;
    self.emailTextField.delegate = self;
    self.errorLabel.alpha = 0.0;
    
    self.view.backgroundColor = [Helper sharedHelper].secondaryColor;
    
    [self.userNameTextField becomeFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    NSLog(@"fired...");
    
    if(textField == self.userNameTextField)
    {
        if(textField.text.length >= 3 && textField.text.length <= 14){
            [self.userNameTextField resignFirstResponder];
            [self.passwordTextField becomeFirstResponder];
            return YES;
        }
        return NO;
    } else if(textField  == self.passwordTextField) {
        if(textField.text.length >= 3 && textField.text.length <= 14){
            [self.userNameTextField resignFirstResponder];
            [self.passwordTextField becomeFirstResponder];
            return YES;
        }
        return NO;
    } else if(textField == self.emailTextField) {
        
        
        
        if([[Helper sharedHelper] validateEmail:self.emailTextField.text]){             NSDictionary *credentials = [NSDictionary dictionaryWithObjects:
  @[
    self.userNameTextField.text,
    self.passwordTextField.text,
    self.emailTextField.text
    ] forKeys:@[
                @"username",
                @"password",
                @"email"]];
            
            [[APIAgent sharedAgent] postToEndpoint:@"auth/register" withParams:credentials onSuccess:^(NSDictionary *user) {
                [[Helper sharedHelper] saveCurrentUser:[[CurrentUser alloc] initWithDictionary:user]];
                NSLog(@"Logged in.");
                [self dismissViewControllerAnimated:YES completion:nil];
            } onFail:^(id error) {
                NSDictionary *d = (NSDictionary*)error;
                if([[[d valueForKey:@"error"] valueForKey:@"code"] integerValue] == 121){
                    // signup failure
                    self.errorLabel.alpha = 1;
                }
                NSLog(@"Error: %@", error);
            }];
            return NO;
        }
        return NO;
    }
    return NO;

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
