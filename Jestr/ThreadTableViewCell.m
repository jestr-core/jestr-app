//
//  ThreadTableViewCell.m
//  Jestr
//
//  Created by Árpád Kiss on 2014.07.02..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import "ThreadTableViewCell.h"
#import "User_Public.h"
#import "UIImageView+AFNetworking.h"

@implementation ThreadTableViewCell

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void)configureCell:(Thread_Preview*)preview {
    NSString *profileImageURL = [@"http://api.jestr.me/"
                                 stringByAppendingString:EMPTYIFNIL(                                                       [preview.friend.profile_picture valueForKey:@"medium"])];
    [self.profileImageView setImageWithURL:[NSURL URLWithString:profileImageURL]];
    self.usernameLabel.text = preview.friend.username;
    [self drawThumbnails:preview.posts];
}

-(void)drawThumbnails:(NSArray*)images {
    __block CGFloat X = self.thumbnailContainerView.bounds.size.width-38;
    
    [images enumerateObjectsUsingBlock:^(NSDictionary * obj, NSUInteger idx, BOOL *stop) {
        if(![obj isEqual:[NSNull null]]) {
            UIImageView *v = [[UIImageView alloc] initWithFrame:CGRectMake(X, 0, 38, 38)];
            NSString* tURL = [@"http://api.jestr.me/"
                          stringByAppendingString:EMPTYIFNIL([obj valueForKey:@"thumbnail"])];
            [v setImageWithURL:[NSURL URLWithString:tURL]];
            [self.thumbnailContainerView addSubview:v];
            X = X - 42;
        }
    }];
}

-(void)drawRect:(CGRect)rect {
    self.profileImageView.layer.cornerRadius = self.profileImageView.bounds.size.width / 2;
    self.profileImageView.layer.masksToBounds = YES;
    self.profileImageView.layer.borderWidth = 1;
    self.profileImageView.layer.borderColor = [[UIColor whiteColor] CGColor];
}

@end
