//
//  RelationViewController.h
//  Jestr
//
//  Created by Árpád Kiss on 2014.07.14..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RelationViewController : UIViewController <
    UITableViewDataSource,
    UITableViewDelegate
>

@property (nonatomic) BOOL type; // YES: follower, NO: following
@property (nonatomic) NSNumber* userId;

@property (weak, nonatomic) IBOutlet UITableView *relationTableView;

@end
