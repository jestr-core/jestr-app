//
//  User_Public.m
//  Jestr
//
//  Created by Árpád Kiss on 2014.07.16..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import "User_Public.h"


@implementation User_Public

@dynamic id;
@dynamic profile_picture;
@dynamic username;

@end
