//
//  User_Public.h
//  Jestr
//
//  Created by Árpád Kiss on 2014.07.16..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface User_Public : NSManagedObject

@property (nonatomic, retain) NSNumber * id;
@property (nonatomic, retain) NSDictionary* profile_picture;
@property (nonatomic, retain) NSString * username;

@end
