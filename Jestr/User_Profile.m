//
//  User_Profile.m
//  Jestr
//
//  Created by Árpád Kiss on 2014.07.15..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import "User_Profile.h"
#import "User_Public.h"


@implementation User_Profile

@dynamic id;
@dynamic private;
@dynamic profile;
@dynamic stats;
@dynamic username;
@dynamic user_public;
@dynamic followers;
@dynamic following;

@end
