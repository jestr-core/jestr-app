//
//  ThreadTableViewCell.h
//  Jestr
//
//  Created by Árpád Kiss on 2014.07.02..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Thread_Preview.h"

@interface ThreadTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UIView *thumbnailContainerView;


-(void)configureCell:(Thread_Preview*)preview;

@end
