//
//  Comment_Public.m
//  Jestr
//
//  Created by Árpád Kiss on 2014.05.17..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import "Comment_Public.h"
#import "User_Public.h"


@implementation Comment_Public

@dynamic id;
@dynamic created_time;
@dynamic message;
@dynamic from;

@end
