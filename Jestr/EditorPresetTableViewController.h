//
//  EditorPresetTableViewController.h
//  Jestr
//
//  Created by Árpád Kiss on 2014.05.20..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

//#import "GPUImage.h"
#import <UIKit/UIKit.h>


@interface EditorPresetTableViewController : UITableViewController

@property(nonatomic, strong) UIImage *croppedImage;

@end
