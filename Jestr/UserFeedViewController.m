//
//  UserFeedTableViewController.m
//  Jestr
//
//  Created by Árpád Kiss on 2014.04.28..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import "UserFeedViewController.h"
#import "Post_Public.h"
#import "Comment_Preview.h"
#import "Comment_Public.h"
#import "StorageManager.h"
#import "APIAgent.h"
#import "Helper.h"
#import "UIImageView+AFNetworking.h"
#import "AVHexColor.h"
#import "ILTranslucentView.h"
#import "User_Public.h"
#import "CommentViewController.h"
#import "ProfileFeedViewController.h"
#import "CoolersViewController.h"

@interface UserFeedViewController () {
    NSArray* _posts;
    NSManagedObjectContext *_tmpContext;
    BOOL _isMenuHidden;
    ILTranslucentView *_blurView;
    NSInteger _menuStep;
    Post_Public* _tmpPost;
    NSIndexPath* _tmpIndexPath;
    UIRefreshControl* _r;
    int _page;
    NSString* _lastEndpoint;
    NSPredicate* _feedPredicate;
}

@end

@implementation UserFeedViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _feedPredicate = [NSPredicate predicateWithFormat:@"type = 1"];
    
    _page = 0;
    
    [self setUpMenu];
    
    _tmpContext = [NSManagedObjectContext MR_defaultContext];
    
    UINib* nib = [UINib nibWithNibName:@"PostPublicTableViewCell" bundle:nil];
    
    [self.tableView registerNib:nib forCellReuseIdentifier:kFeedCellReuseIdentifier];
    
    [self fetchPosts];
    
    _blurView = [[ILTranslucentView alloc] initWithFrame:self.view.bounds];
    _blurView.translucentAlpha = 0;
    _blurView.translucentStyle = UIBarStyleBlackTranslucent;
    _blurView.translucentTintColor = [UIColor clearColor];
    _blurView.backgroundColor = [UIColor clearColor];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notifReceived:) name:@"pushNotifReceivedFromAPI" object:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didDismissSecondViewController)
                                                 name:@"EditorNavigationControllerDismissed"
                                               object:nil];
    
    _r = [[UIRefreshControl alloc] init];
    _r.tintColor = [Helper sharedHelper].secondaryColor;
    [_r addTarget:self action:@selector(refreshFeed) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:_r];
    
    
    UITapGestureRecognizer* tapRecon = [[UITapGestureRecognizer alloc]
                                        initWithTarget:self action:@selector(navigationBarDoubleTap:)];
    tapRecon.numberOfTapsRequired = 2;
    [self.navigationController.navigationBar addGestureRecognizer:tapRecon];
}

- (void)navigationBarDoubleTap:(UIGestureRecognizer*)recognizer {
    [self.tableView setContentOffset:CGPointMake(0,0) animated:YES];
}

-(void)notifReceived:(NSNotification*)object {
    [self performSegueWithIdentifier:@"ToNotificationsFromFeed" sender:nil];
    NSLog(@"%@", object);
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self fixMenuPositions];
    [self setUpRecognizer];
    [self checkTempPost];
}

#pragma mark Tableview gesture recognizer

-(void)setUpRecognizer {
    UISwipeGestureRecognizer *r = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(didSwipe:)];
    [r setDirection:UISwipeGestureRecognizerDirectionRight];
    [self.tableView addGestureRecognizer:r];
    r = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(didSwipe:)];
    [r setDirection:UISwipeGestureRecognizerDirectionLeft];
    [self.tableView addGestureRecognizer:r];

}

- (void)didSwipe:(UIGestureRecognizer *)gestureRecognizer {
    CGPoint swipeLocation = [gestureRecognizer locationInView:self.tableView];
    NSIndexPath *swipedIndexPath = [self.tableView indexPathForRowAtPoint:swipeLocation];
    PostPublicTableViewCell* swipedCell = (PostPublicTableViewCell*)[self.tableView cellForRowAtIndexPath:swipedIndexPath];
    [swipedCell performSwipe:gestureRecognizer];
    NSLog(@"triggered swipe on %@", swipedCell);
    NSLog(@"%@", gestureRecognizer);
}

#pragma mark Post related stuff

-(void)checkTempPost {
    if(_tmpPost != nil) {
        NSString* postEndpoint = [@"posts/" stringByAppendingString:_tmpPost.id.description];
        [[APIAgent sharedAgent] getFromEndpoint:postEndpoint onSuccess:^(NSDictionary* responseObject) {
            //NSDictionary* r = (NSDictionary*)responseObject;
            NSDictionary* cl = [responseObject objectForKey:@"comments"];
            
            Comment_Preview* cp = [Comment_Preview MR_createInContext:_tmpContext];
            
            //
            //Comment_Preview* cp = [Comment_Preview MR_importFromObject:responseObject inContext:_tmpContext];
            //return;
            
            NSArray* comments = [cl objectForKey:@"data"];
            
            cp.count = [cl objectForKey:@"count"];
            
            NSMutableSet* tSet = [[NSMutableSet alloc] init];
            
            if(![comments isEqual:[NSNull null]]) {
            [comments enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                NSDictionary* current = (NSDictionary*)obj;
                NSDictionary* from = [current objectForKey:@"from"];
                
                Comment_Public* cl = [Comment_Public MR_createInContext:_tmpContext];
                cl.id = [current objectForKey:@"id"];
                cl.message = [current valueForKey:@"message"];
                
                cl.created_time = [current objectForKey:@"created_time"];
                
                User_Public *p = [User_Public MR_createInContext:_tmpContext];
                p.id = [from objectForKey:@"id"];
                p.username = [from valueForKey:@"username"];
                p.profile_picture = NILIFNULL([from objectForKey:@"profile_picture"]);
                
                cl.from = p;
                
                [tSet addObject:cl];
            }];
            
            cp.data = [NSSet setWithSet:tSet];
            }
            
            NSDictionary* coolz = [responseObject objectForKey:@"coolerz"];
            
            Cool_Preview* cool = [Cool_Preview MR_createInContext:_tmpContext];
            
            cool.count = [coolz objectForKey:@"count"];

            NSArray* coolerz = [coolz objectForKey:@"data"];
            
            tSet = [[NSMutableSet alloc] init];
            
            if(![coolerz isEqual:[NSNull null]]) {

            
            [coolerz enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                [tSet addObject:[User_Public MR_importFromObject:obj inContext:_tmpContext]];
            }];
            
            cool.data = [NSSet setWithSet:tSet];
            }

            NSLog(@"%@", responseObject);
            
            //Comment_Preview* cp = [Comment_Preview MR_importFromObject:[responseObject objectForKey:@"comments"] inContext:_tmpContext];
            
            _tmpPost.comments = cp;
            _tmpPost.coolerz = cool;
            
            [self.tableView reloadRowsAtIndexPaths:@[_tmpIndexPath] withRowAnimation:UITableViewRowAnimationNone];
            
        } onFail:^(id error) {
            
        }];
    }
}

-(void)fetchPosts {
    _posts = [Post_Public MR_findAllSortedBy:@"created_time" ascending:NO withPredicate:_feedPredicate inContext:_tmpContext];
    
    [self.tableView reloadData];
    
    [self loadPosts];
}

-(void)refreshFeed {
    _page = 0;
    [Post_Public MR_deleteAllMatchingPredicate:_feedPredicate inContext:_tmpContext];
    [self loadPosts];
}

-(void)loadPosts {
    NSString* feedUrl = [@"users/me/feed?page=" stringByAppendingFormat:@"%i", _page];
    
    _lastEndpoint = feedUrl;
    
    [[APIAgent sharedAgent] getFromEndpoint:feedUrl onSuccess:^(id responseObject) {
        NSLog(@"DATA: %@", responseObject);
        
        if(![responseObject isEqual:[NSNull null]]) {
            
            __block NSPredicate* p;
            __block NSArray* pos;
            __block Post_Public* post;
            [responseObject enumerateObjectsUsingBlock:^(NSDictionary* obj, NSUInteger idx, BOOL *stop) {
                p = [NSPredicate predicateWithFormat:@"id = %d", [obj valueForKey:@"id"]];
                pos = [Post_Public MR_findAllWithPredicate:p inContext:_tmpContext];
                
                if(pos.count == 0) {
                    post = [Post_Public MR_importFromObject:obj inContext:_tmpContext];
                    post.type = @1;
                }
            }];
            
            //_posts = [Post_Public MR_findAllSortedBy:@"created_time" ascending:NO inContext:_tmpContext];
            
            _posts = [Post_Public MR_findAllSortedBy:@"created_time" ascending:NO withPredicate:_feedPredicate inContext:_tmpContext];
            
            [self.tableView reloadData];
            [_r endRefreshing];
        } else {
            [_r endRefreshing];
            _page = (_page == 0) ? 0 : _page - 1;
        }
        
    } onFail:^(NSError *error) {
        NSLog(@"%@",error);
    }];

}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark TableView delegate functions

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _posts.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
     PostPublicTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:kFeedCellReuseIdentifier];
     
     [cell forceResetCell];
     
     NSInteger idx = indexPath.row;
     
     Post_Public *row = _posts[idx];
     
     ProfileActionType type = ([row.author.id isEqual:[Helper sharedHelper].currentUser.id]) ? own : friend;
     
     cell.post = row;
     
     [cell configureCellforType:type withIndexPath:indexPath];
     
     cell.delegate = self;
 
     return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row == _posts.count - 1) {
        _page = _page + 1;
        [self loadPosts];
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 320;
}

#pragma mark Menu stuff

-(void)setUpMenu {
    
    _menuStep = 50;
    
    _isMenuHidden = YES;
    
    [self.toggleMenuButton setImage:[UIImage imageNamed:@"menuOpen.png"] forState:UIControlStateNormal];
    [self.toggleMenuButton setImage:[UIImage imageNamed:@"menuClose.png"] forState:UIControlStateSelected];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    self.menuView.backgroundColor = [Helper sharedHelper].secondaryColor;
    [self.menuButtons enumerateObjectsUsingBlock:^(MenuButton* obj, NSUInteger idx, BOOL *stop) {
        switch (idx) {
            case 0: // notif
                [obj setImage:[UIImage imageNamed:@"menuIconNotif.png"] forState:UIControlStateNormal];
                break;
            case 1: // profile
                [obj setImage:[UIImage imageNamed:@"menuIconProfile.png"] forState:UIControlStateNormal];
                break;
            case 2: // messages
                [obj setImage:[UIImage imageNamed:@"menuIconMessages.png"] forState:UIControlStateNormal];
                break;
            case 3: // explore
                [obj setImage:[UIImage imageNamed:@"menuIconExplore.png"] forState:UIControlStateNormal];
                break;
            case 4: // search
                [obj setImage:[UIImage imageNamed:@"menuIconSearch.png"] forState:UIControlStateNormal];
                break;
            default:
                break;
        }
    }];
}

-(void)fixMenuPositions {
    [self.menuButtons enumerateObjectsUsingBlock:^(MenuButton* obj, NSUInteger idx, BOOL *stop) {
        obj.frame = CGRectMake(0, obj.frame.origin.y + (5 - idx) * _menuStep, obj.frame.size.width, obj.frame.size.height);
    }];
}

- (IBAction)toggleMenu:(UIButton *)sender {
    sender.selected = !sender.selected;
    if(sender.selected) {
        [self.feedContainerView addSubview:_blurView];
        float top = self.view.frame.size.height - 360;
        self.tableView.scrollEnabled = NO;
        //[self.tableView setContentOffset:self.tableView.contentOffset animated:NO];
        _isMenuHidden = NO;
        [UIView animateWithDuration:0.5
                              delay:0.0
             usingSpringWithDamping:1.0
              initialSpringVelocity:4.0
                            options: UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             //_blurView.alpha = 1;
                             _blurView.translucentAlpha = 1;
                             self.menuButtonsView.frame = CGRectMake(0, top, 320, 300);
                             [self.menuButtons enumerateObjectsUsingBlock:^(MenuButton* obj, NSUInteger idx, BOOL *stop) {
                                 obj.frame = CGRectMake(0, obj.frame.origin.y - (5 - idx) * _menuStep, obj.frame.size.width, obj.frame.size.height);
                             }];
                         }
                         completion:^(BOOL finished){

                         }];
        [UIView commitAnimations];
    } else {
        _isMenuHidden = YES;
        self.tableView.scrollEnabled = YES;
        [UIView animateWithDuration:0.5
                              delay:0.0
             usingSpringWithDamping:1.0
              initialSpringVelocity:4.0
                            options: UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             //_blurView.alpha = 0;
                             _blurView.translucentAlpha = 0;
                             self.menuButtonsView.frame = CGRectMake(0, self.view.frame.size.height, 320, 300);
                             [self.menuButtons enumerateObjectsUsingBlock:^(MenuButton* obj, NSUInteger idx, BOOL *stop) {
                                 obj.frame = CGRectMake(0, obj.frame.origin.y + (5 - idx) * _menuStep, obj.frame.size.width, obj.frame.size.height);
                             }];
                         }
                         completion:^(BOOL finished){
                             if(_isMenuHidden){
                                 [_blurView removeFromSuperview];
                             }
                         }];
        [UIView commitAnimations];
    }
}

-(IBAction)menuSelected:(MenuButton*)sender {
    NSLog(@"%@", sender);
    if([sender.titleLabel.text isEqualToString:@"PROFILE"]) {
         [self performSegueWithIdentifier:@"ToProfileFeedView" sender:sender];
    } else if([sender.titleLabel.text isEqualToString:@"MESSAGES"]) {
        [self performSegueWithIdentifier:@"ToMessageThreadsFromFeed" sender:nil];
    } else if([sender.titleLabel.text isEqualToString:@"EXPLORE"]) {
        [self performSegueWithIdentifier:@"ToTrendingFromFeed" sender:nil];
    } else if([sender.titleLabel.text isEqualToString:@"SEARCH"]) {
        [self performSegueWithIdentifier:@"ToSearchFromFeed" sender:nil];
    } else if([sender.titleLabel.text isEqualToString:@"NOTIFICATIONS"]) {
        [self performSegueWithIdentifier:@"ToNotificationsFromFeed" sender:nil];
    }
}

#pragma mark Editor actions

- (IBAction)openEditor:(id)sender {
    if(!_isMenuHidden) {
        [self toggleMenu:self.toggleMenuButton];
    }
    UINavigationController *lv = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"EditorNavigationControllerID"];
    lv.navigationBar.translucent = NO;
    [lv setModalPresentationStyle:UIModalPresentationFullScreen];
    [self presentViewController:lv
                       animated:YES
                     completion:nil];
    
}

-(void)didDismissSecondViewController {
//    [[APIAgent sharedAgent] getFromEndpoint:@"users/me/feed" onSuccess:^(id responseObject) {
//        NSLog(@"DATA: %@", responseObject);
//        
//        if(![responseObject isEqual:[NSNull null]]) {
//            
//            [responseObject enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
//                [Post_Public MR_importFromObject:obj inContext:_tmpContext];
//            }];
//            
//            _posts = [Post_Public MR_findAllSortedBy:@"created_time" ascending:NO inContext:_tmpContext];
//            
//            [self.tableView reloadData];
//            //[self.tableView scrollToRowAtIndexPath:0  atScrollPosition:UITableViewScrollPositionTop animated:YES];
//            [self.tableView setContentOffset:CGPointZero animated:YES];
//        }
//        
//    } onFail:^(NSError *error) {
//        NSLog(@"%@",error);
//    }];
}

#pragma mark Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if(!_isMenuHidden) {
        [self toggleMenu:self.toggleMenuButton];
    }
    if([segue.identifier isEqual:@"ToCommentFromFeed"]) {
        CommentViewController* c = (CommentViewController*)segue.destinationViewController;
        c.post = _tmpPost;
    } else if([segue.identifier isEqual:@"ToProfileFeedView"]) {
        ProfileFeedViewController* c = (ProfileFeedViewController*)segue.destinationViewController;
        if(!sender) {
            c.public = _tmpPost.author;
        } else if([sender isKindOfClass:[Comment_Public class]]) {
            Comment_Public* co = (Comment_Public*)sender;
            c.public = co.from;
        }
    } else if([segue.identifier isEqual:@"ToCoolersFromFeed"]) {
        CoolersViewController* c = (CoolersViewController*)segue.destinationViewController;
        c.postId = _tmpPost.id;
        c.preview = _tmpPost.coolerz;
    }
}

#pragma mark Post delegate methods

-(void)onCommentButtonPress:(Post_Public*)post
                 withSender:(PostPublicTableViewCell*)cell
               andIndexPath:(NSIndexPath*)path
{
    _tmpIndexPath = path;
    _tmpPost = post;
    [cell resetCell];
    [self performSegueWithIdentifier:@"ToCommentFromFeed" sender:nil];
}

-(void)onProfileAction:(Post_Public*)post
        withActionType:(ProfileActionType)type
             andSender:(PostPublicTableViewCell*)cell
          andIndexPath:(NSIndexPath*)path
{
    switch (type) {
        case friend: {
            // unfollow
            NSString* followEndpoint = [[@"users/" stringByAppendingString:post.author.id.description] stringByAppendingString:@"/followers"];
            
            [[APIAgent sharedAgent] deleteFromEndpoint: followEndpoint withParams:@{} onSuccess:^(id responseObject) {
                //code
                [self refreshFeed];
            } onFail:^(id error) {
                //code
            }];
            break;
        }
        case own: {
            //deletePost
            _tmpPost = post;
            _tmpIndexPath = path;
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle:@"Confirm!"
                                  message:@"Are you sure you want to delete this post?"
                                  delegate: self
                                  cancelButtonTitle:@"Cancel"
                                  otherButtonTitles:@"OK", nil];
            [alert show];
            break;
        }
        case unknown: {
            // follow
            NSString* followEndpoint = [[@"users/" stringByAppendingString:post.author.id.description] stringByAppendingString:@"/followers"];
            
            [[APIAgent sharedAgent] postToEndpoint: followEndpoint withParams:@{} onSuccess:^(id responseObject) {
                //code
            } onFail:^(id error) {
                //code
            }];
            break;
        }
        default:
            break;
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if(buttonIndex == 1)
    {
        NSString* deletePostEndpoint = [@"posts/" stringByAppendingString:_tmpPost.id.description];
        [[APIAgent sharedAgent] deleteFromEndpoint: deletePostEndpoint withParams:@{} onSuccess:^(id responseObject) {
            //code
            [_tmpPost MR_deleteEntity];
            [self fetchPosts];
            [self.tableView reloadData];
        } onFail:^(id error) {
            //code
        }];
    }
    
}


-(void)onCommentsLabelTouched:(Post_Public*)post
                    withSender:(PostPublicTableViewCell*)cell
                 andIndexPath:(NSIndexPath*)path
{
    NSLog(@"comments label tapped.");
    _tmpIndexPath = path;
    _tmpPost = post;
    [cell resetCell];
    [self performSegueWithIdentifier:@"ToCommentFromFeed" sender:nil];

}

-(void)onCoolersLabelTouched:(Post_Public*)post
                   withSender:(PostPublicTableViewCell*)cell
                andIndexPath:(NSIndexPath*)path
{
    NSLog(@"coolers label tapped.");
    _tmpIndexPath = path;
    _tmpPost = post;
    [cell resetCell];
    [self performSegueWithIdentifier:@"ToCoolersFromFeed" sender:nil];
}

-(void)onUsernameLabelTouched:(Post_Public*)post
                   withSender:(PostPublicTableViewCell*)cell
                 andIndexPath:(NSIndexPath*)path {
    NSLog(@"username label tapped.");
    _tmpIndexPath = path;
    _tmpPost = post;
    [cell resetCell];
    [self performSegueWithIdentifier:@"ToProfileFeedView" sender:nil];

}

-(void)onDoubleTap:(Post_Public *)post withSender:(PostPublicTableViewCell *)cell andIndexPath:(NSIndexPath *)path {
    
    NSString* postEndpoint = [[@"posts/" stringByAppendingString:post.id.description] stringByAppendingString:@"/coolers"];
    
    _tmpPost = post;
    _tmpIndexPath = path;
    
    [[APIAgent sharedAgent] postToEndpoint:postEndpoint withParams:@{} onSuccess:^(NSArray* responseObject) {
        
        // todo
        [self checkTempPost];
        
    } onFail:^(id error) {
        NSLog(@"%@", error);
    }];
    
}

-(void)onCommentUsernameLabelTouched:(Comment_Public *)comment withPost:(Post_Public *)post withSender:(PostPublicTableViewCell *)cell andIndexPath:(NSIndexPath *)path {
    _tmpIndexPath = path;
    _tmpPost = post;
    [self performSegueWithIdentifier:@"ToProfileFeedView" sender:comment];
}

@end
