//
//  UserFeedTableViewController.h
//  Jestr
//
//  Created by Árpád Kiss on 2014.04.28..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MenuButton.h"
#import "PostPublicTableViewCell.h"

@interface UserFeedViewController : UIViewController <
    UITableViewDelegate,
    UITableViewDataSource,
    PostPublicTableViewCellDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *menuView;
@property (weak, nonatomic) IBOutlet UIButton *toggleMenuButton;
@property (weak, nonatomic) IBOutlet UIView *menuButtonsView;
@property (weak, nonatomic) IBOutlet UIView *feedContainerView;
@property (nonatomic) IBOutletCollection(MenuButton) NSArray *menuButtons;

@end
