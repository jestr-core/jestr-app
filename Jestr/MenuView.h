//
//  MenuView.h
//  Jestr
//
//  Created by Árpád Kiss on 2014.07.11..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MenuButton.h"

typedef NS_ENUM(NSUInteger, MenuButtonType) {
    notifications,
    profile,
    messages,
    explore,
    search
};

@interface MenuView : UIView

@property (nonatomic, assign) id delegate;

@property (weak, nonatomic) IBOutlet UIButton *toggleMenuButton;
@property (weak, nonatomic) IBOutlet UIButton *editorButton;

@property (strong, nonatomic) IBOutletCollection(MenuButton) NSArray *menuButtons;

@end

@protocol MenuViewDelegate

-(void)menuButtonPressed:(MenuButtonType)type;
-(void)editorButtonPressed;
-(void)menuToggled:(BOOL)state;

@end
