//
//  Comment_Preview.h
//  Jestr
//
//  Created by Árpád Kiss on 2014.05.17..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Comment_Public;

@interface Comment_Preview : NSManagedObject

@property (nonatomic, retain) NSNumber * count;
@property (nonatomic, retain) NSSet *data;

@end

@interface Comment_Preview (CoreDataGeneratedAccessors)

- (void)addDataObject:(Comment_Public *)value;
- (void)removeDataObject:(Comment_Public *)value;
- (void)addData:(NSSet *)values;
- (void)removeData:(NSSet *)values;

@end
