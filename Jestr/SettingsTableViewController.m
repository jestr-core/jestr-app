//
//  SettingsTableViewController.m
//  Jestr
//
//  Created by Árpád Kiss on 2014.07.02..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import "SettingsTableViewController.h"

@interface SettingsTableViewController () {
    NSArray* _settings;
}

@end

@implementation SettingsTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"Settings";
    
    _settings = @[
                  @"Support",
                  @"Report problem",
                  @"General feedback",
                  @"Find ppl2follow",
                  @"Help center",
                  @"Privacy policy",
                  @"Terms of Service",
                  @"About this version",
                  @"Push notif",
                  @"Save original photos",
                  @"Clear search history",
                  @"Logout"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return _settings.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SettingsCellPrototype" forIndexPath:indexPath];
    
    cell.textLabel.text = _settings[indexPath.row];
    
    return cell;
}


@end
