//
//  Post_Message.h
//  Jestr
//
//  Created by Árpád Kiss on 2014.07.22..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class User_Public;

@interface Post_Message : NSManagedObject

@property (nonatomic, retain) NSNumber * created_time;
@property (nonatomic, retain) NSNumber * id;
@property (nonatomic, retain) id images;
@property (nonatomic, retain) NSNumber * relatedTo;
@property (nonatomic, retain) User_Public *author;

@end
