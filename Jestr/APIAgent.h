//
//  APIAgent.h
//  Jestr
//
//  Created by Árpád Kiss on 2014.04.27..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking/AFNetworking.h"
#import "CurrentUser.h"

@interface APIAgent : NSObject

+(APIAgent*) sharedAgent;

-(void)postToEndpoint:(NSString*)endpoint
           withParams:(NSDictionary*)parameters
            onSuccess:(void(^)(id responseObject))successBlock
               onFail:(void(^)(id error))failBlock;

-(void)postToEndpoint:(NSString*)endpoint
           withParams:(NSDictionary*)parameters
            withImage:(UIImage *)image
              andName:(NSString *)name
            onSuccess: (void(^)(id responseObject))successBlock
               onFail: (void(^)(NSError *error))failBlock;

-(void)postToEndpoint:(NSString*)endpoint
           withParams:(NSDictionary*)parameters
           withImages:(NSArray*)images
            onSuccess: (void(^)(id responseObject))successBlock
               onFail: (void(^)(NSError *error))failBlock;

-(void)getFromEndpoint:(NSString*)endpoint
            onSuccess:(void(^)(id responseObject))successBlock
               onFail:(void(^)(id error))failBlock;

-(void)putOnEndpoint:(NSString*)endpoint
           withParams:(NSDictionary*)parameters
            onSuccess:(void(^)(id responseObject))successBlock
               onFail:(void(^)(id error))failBlock;

-(void)deleteFromEndpoint:(NSString*)endpoint
           withParams:(NSDictionary*)parameters
            onSuccess:(void(^)(id responseObject))successBlock
               onFail:(void(^)(id error))failBlock;

-(BOOL)isUserHaveSession;
-(void)resetSession;
-(void)registerUserToken:(NSString*)token;
@end
