//
//  NotificationsViewController.h
//  Jestr
//
//  Created by Árpád Kiss on 2014.07.03..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NotifUserInteractionTableViewCell.h"

@interface NotificationsViewController : UIViewController <UITableViewDataSource,
    UITableViewDelegate,
    NotifUserInteractionDelegate
>

@property (weak, nonatomic) IBOutlet UITableView *notificationsTableView;

@end
