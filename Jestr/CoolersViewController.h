//
//  CoolersViewController.h
//  Jestr
//
//  Created by Árpád Kiss on 2014.07.01..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Cool_Preview.h"

@interface CoolersViewController : UIViewController <
UITableViewDelegate,
UITableViewDataSource
>

@property (strong, nonatomic) NSNumber* postId;

@property (strong, nonatomic) Cool_Preview* preview;

@property (weak, nonatomic) IBOutlet UITableView *userListTableView;

@end
