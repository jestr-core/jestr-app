//
//  Comment_Public.h
//  Jestr
//
//  Created by Árpád Kiss on 2014.06.09..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Comment_Preview, User_Public;

@interface Comment_Public : NSManagedObject

@property (nonatomic, retain) NSNumber * created_time;
@property (nonatomic, retain) NSNumber * id;
@property (nonatomic, retain) NSString * message;
@property (nonatomic, retain) User_Public *from;
@property (nonatomic, retain) Comment_Preview *comment_preview;

@end
