//
//  CommentsPreviewTableViewController.h
//  Jestr
//
//  Created by Árpád Kiss on 2014.05.17..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentsPreviewTableViewController : UITableViewController

@end
