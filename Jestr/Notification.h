//
//  Notification.h
//  Jestr
//
//  Created by Árpád Kiss on 2014.07.01..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class User_Public;

//typedef NS_ENUM(NSUInteger, NotificationType) {
//    follow_request,
//    follow_request_confirmation,
//    new_private_message,
//    user_commented_on_post,
//    user_cooled_on_post,
//    friend_joined
//};

@interface Notification : NSManagedObject

@property (nonatomic, retain) NSNumber * id;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSNumber * confirmed;
@property (nonatomic, retain) NSDictionary* related_object;
@property (nonatomic, retain) NSNumber * created_time;
@property (nonatomic, retain) User_Public *from;

@end
