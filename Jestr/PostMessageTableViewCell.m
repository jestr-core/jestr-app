//
//  PostMessageTableViewCell.m
//  Jestr
//
//  Created by Árpád Kiss on 2014.07.02..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import "PostMessageTableViewCell.h"
#import "User_Public.h"
#import "UIImageView+AFNetworking.h"
#import "Helper.h"

@implementation PostMessageTableViewCell {
    Post_Message* _message;
}

- (void)awakeFromNib
{
    // Initialization code
    self.separatorInset = UIEdgeInsetsMake(0, 0, 0, self.bounds.size.width);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

//-(void)configureCell:(Post_Message*)message {
//    NSString *profileImageURL = [@"http://api.jestr.me/"
//                                 stringByAppendingString:EMPTYIFNIL(                                                       [preview.friend.profile_picture valueForKey:@"medium"])];
//    [self.profileImageView setImageWithURL:[NSURL URLWithString:profileImageURL]];
//
//}

-(void)configureCell:(Post_Message*)message {
    _message = message;
    NSString *profileImageURL = [@"http://api.jestr.me/"
                                 stringByAppendingString:EMPTYIFNIL(                                                       [_message.author.profile_picture valueForKey:@"medium"])];
    [self.profileImageView setImageWithURL:[NSURL URLWithString:profileImageURL]];
    
    
    NSString *postImageURL = [@"http://api.jestr.me/"
                             stringByAppendingString:EMPTYIFNIL(                                                       [[_message.images objectForKey:@"cropped"] valueForKey:@"standard"])];
    [self.postImageView setImageWithURL:[NSURL URLWithString:postImageURL]];
    self.dateLabel.text = [[Helper sharedHelper] dateDiff:[_message.created_time doubleValue]];
    [self.dateLabel sizeToFit];
}

-(void)forceResetCell {
    self.profileImageView.image = nil;
    self.postImageView.image = nil;
}

-(void)drawRect:(CGRect)rect {
    self.profileImageView.layer.cornerRadius = self.profileImageView.bounds.size.width / 2;
    self.profileImageView.layer.masksToBounds = YES;
    self.profileImageView.layer.borderWidth = 1;
    self.profileImageView.layer.borderColor = [[UIColor whiteColor] CGColor];
//    if(![_message.author.id isEqual:[Helper sharedHelper].currentUser.id]) {
//        
//        NSLog(@"%@", [Helper sharedHelper].currentUser.id);
//        NSLog(@"%@", _message.author.id);
//        
//        
//        CGFloat left = 320-self.profileImageView.frame.size.width - self.profileImageView.frame.origin.x;
//        self.profileImageView.frame = CGRectMake(left, self.profileImageView.frame.origin.y, self.profileImageView.frame.size.width, self.profileImageView.frame.size.height);
//        
//        NSLog(@"%@", NSStringFromCGRect(self.profileImageView.frame));
//    } else {
//        self.profileImageView.frame = CGRectMake(10, 10, 40, 40);
//    }
}

@end
