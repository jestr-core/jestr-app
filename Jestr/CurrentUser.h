//
//  CurrentUser.h
//  Jestr
//
//  Created by Árpád Kiss on 2014.04.28..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CurrentUser : NSObject <NSCoding>

@property (nonatomic) NSNumber* id;
@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSMutableDictionary *profile;
@property (nonatomic) BOOL private;
@property (nonatomic) BOOL location;
@property (nonatomic) NSUInteger lastLogin;
@property (nonatomic) NSUInteger lastUpdate;

- (id)init;
- (instancetype)initWithDictionary:(NSDictionary *)object;

- (instancetype)initWithId:(NSNumber*)newId
                     Email:(NSString*)newEmail
                  Username:(NSString*)newUsername
                   Profile:(NSMutableDictionary*)newProfile
                  Location:(BOOL)newLocation
                   Private:(BOOL)newPrivate
                 LastLogin:(NSUInteger)newLastLogin
                LastUpdate:(NSUInteger)newLastUpdate;

@end
