//
//  PresetTableViewCell.h
//  Jestr
//
//  Created by Árpád Kiss on 2014.05.21..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Post_Public.h"

@interface PostPublicTableViewCell : UITableViewCell <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (weak, nonatomic) IBOutlet UITableView *commentPreTableView;
@property (weak, nonatomic) IBOutlet UIView *profilePreviewView;
@property (weak, nonatomic) IBOutlet UIButton *profilePreviewActionButton;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UILabel *commentsLabel;
@property (weak, nonatomic) IBOutlet UILabel *coolLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (nonatomic, strong) Post_Public *post;
@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;

@end
