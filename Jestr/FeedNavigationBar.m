//
//  FeedNavigationBar.m
//  Jestr
//
//  Created by Árpád Kiss on 2014.05.19..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import "FeedNavigationBar.h"

@implementation FeedNavigationBar

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (CGSize)sizeThatFits:(CGSize)size {
    return CGSizeMake(320,1);
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
