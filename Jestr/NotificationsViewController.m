//
//  NotificationsViewController.m
//  Jestr
//
//  Created by Árpád Kiss on 2014.07.03..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import "NotificationsViewController.h"
#import "APIAgent.h"
#import "Notification.h"
#import "User_Public.h"
#import "Post_Public.h"
#import "NotifInteractionOnPostTableViewCell.h"

#import "ProfileFeedViewController.h"
#import "CommentViewController.h"
#import "UserPrivateThreadViewController.h"

@interface NotificationsViewController () {
    NSArray* _notifications;
    NSManagedObjectContext* _tmpContext;
    NSDictionary* _notifLabels;
    Notification* _tmpNotif;
}

@end

@implementation NotificationsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _tmpContext = [NSManagedObjectContext MR_contextWithParent:[NSManagedObjectContext MR_contextForCurrentThread]];
    
    _notifLabels = @{
                     @"follow_request": @"{username} wants to follow you",
                     @"follow_request_confirmation": @"Your friend, {username} has confirmed your following request",
                     @"new_private_message": @"You have got a new message from {username}",
                     @"user_commented_on_post": @"{username} has commented on your post",
                     @"user_cooled_on_post": @"{username} has cooled your post",
                     @"friend_joined": @"Your friend, {username} has joined to Jestr",
                     @"mentioned_you": @"{username} has mentioned you in a comment",
                     @"started_following_you": @"{username} has started following you"
                     };
    
    UINib* nib = [UINib nibWithNibName:@"NotifInteractionOnPostTableViewCell" bundle:nil];
    [self.notificationsTableView registerNib:nib forCellReuseIdentifier:kNotificationInteractionIdentifier];
    
    nib = [UINib nibWithNibName:@"NotifUserInteractionTableViewCell" bundle:nil];
    [self.notificationsTableView registerNib:nib forCellReuseIdentifier:kNotificationUserInteractionIdentifier];
    
    [self fetchNotifications];
}

-(NSString*)makeMessageByType:(NSString*)type andUserName:(NSString*)username {
    NSString* message = [_notifLabels valueForKey:type];
    message = [message stringByReplacingOccurrencesOfString:@"{username}" withString:username];
    return message;
}

-(void)fetchNotifications {
    [[APIAgent sharedAgent] getFromEndpoint:@"users/me/notifications" onSuccess:^(id responseObject) {
        
        NSLog(@"%@", responseObject);
        
        if(NILIFNULL(responseObject) == nil){
            _notifications = [[NSArray alloc] init];
            return;
        }
        
        [responseObject enumerateObjectsUsingBlock:^(NSDictionary* obj, NSUInteger idx, BOOL *stop) {
            
            [Notification MR_importFromObject:obj inContext:_tmpContext];
        }];
        
        _notifications = [Notification MR_findAllSortedBy:@"created_time" ascending:NO inContext:_tmpContext];
        
        [self.notificationsTableView reloadData];
        
    } onFail:^(id error) {
        
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _notifications.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Notification* notif = _notifications[indexPath.row];
    if([notif.type isEqualToString:@"follow_request"] ||
       [notif.type isEqualToString:@"follow_request_confirmation"] ||
       [notif.type isEqualToString:@"friend_joined"]) {
        // useres cella
        NotifUserInteractionTableViewCell* cell = [self.notificationsTableView dequeueReusableCellWithIdentifier:kNotificationUserInteractionIdentifier];
        
        [cell configureCell:notif withMessage:[self makeMessageByType:notif.type andUserName:notif.from.username]];
        
        cell.delegate = self;
        
        return cell;
    } else {
        // posztos cella
        
        NotifInteractionOnPostTableViewCell* cell = [self.notificationsTableView dequeueReusableCellWithIdentifier:kNotificationInteractionIdentifier];
        
        [cell configureCell:notif withMessage:[self makeMessageByType:notif.type andUserName:notif.from.username]];
        
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Notification* c = (Notification*)_notifications[indexPath.row];
    
    CGRect r = [[self makeMessageByType:c.type andUserName:c.from.username] boundingRectWithSize:CGSizeMake(204, 0)
                                       options:NSStringDrawingUsesLineFragmentOrigin
                                    attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14]}
                                       context:nil];
    
    CGFloat p = 60 + r.size.height - 40;
    
    return p < 60 ? 60 : p;
}

-(void)tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    _tmpNotif = _notifications[indexPath.row];
    if([_tmpNotif.type isEqualToString:@"follow_request"] ||
       [_tmpNotif.type isEqualToString:@"follow_request_confirmation"] ||
       [_tmpNotif.type isEqualToString:@"friend_joined"]) {
        [self performSegueWithIdentifier:@"ToProfileFromNotif" sender:nil];
        
    } else if([_tmpNotif.type isEqualToString:@"new_private_message"]){
        [self performSegueWithIdentifier:@"ToPrivateThreadFromNotif" sender:nil];
    } else {
        [self performSegueWithIdentifier:@"ToCommentFromNotif" sender:nil];
    }
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    // ToPrivateThreadFromNotif
    // ToProfileFromNotif
    // ToCommentFromNotif
    
    if([segue.identifier isEqualToString:@"ToPrivateThreadFromNotif"]) {
        UserPrivateThreadViewController* vc = (UserPrivateThreadViewController*)segue.destinationViewController;
        vc.friend = _tmpNotif.from;
    } else if([segue.identifier isEqualToString:@"ToProfileFromNotif"]) {
        ProfileFeedViewController* vc = (ProfileFeedViewController*)segue.destinationViewController;
        vc.public = _tmpNotif.from;
    } else if([segue.identifier isEqualToString:@"ToCommentFromNotif"]) {
        CommentViewController* vc = (CommentViewController*)segue.destinationViewController;
        vc.post = [Post_Public MR_importFromObject:_tmpNotif.related_object];
    }
    
}

-(void)onButtonClick:(Notification *)notif {
    NSString* endpoint = @"users/me/notifications";
    [[APIAgent sharedAgent] putOnEndpoint:endpoint withParams:@{@"notif_id":notif.id} onSuccess:^(id responseObject) {
        //code
    } onFail:^(id error) {
        //code
    }];
}


@end
