//
//  EditorChangeMethodViewController.h
//  Jestr
//
//  Created by Árpád Kiss on 2014.07.09..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditorChangeMethodViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *postView;
@property (weak, nonatomic) IBOutlet UIView *sendView;

@end
