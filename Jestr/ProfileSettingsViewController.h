//
//  ProfileSettingsViewController.h
//  Jestr
//
//  Created by Árpád Kiss on 2014.07.29..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User_Profile.h"

@interface ProfileSettingsViewController : UIViewController

@property(nonatomic, strong) User_Profile* profile;
@property(nonatomic, strong) NSManagedObjectContext* inheritedContext;

@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;

@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;

@property (weak, nonatomic) IBOutlet UITextField *emailTextField;

@property (weak, nonatomic) IBOutlet UISwitch *privateSwitch;

@end
