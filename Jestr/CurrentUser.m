//
//  CurrentUser.m
//  Jestr
//
//  Created by Árpád Kiss on 2014.04.28..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import "CurrentUser.h"

#pragma mark CurrentUser encoder keys 
static NSString * const kIdKey = @"UserId";
static NSString * const kUsernameKey = @"Username";
static NSString * const kEmailKey = @"Email";
static NSString * const kLastUpdateKey = @"LastUpdate";
static NSString * const kLastLoginKey = @"LastLogin";
static NSString * const kLocationKey = @"Location";
static NSString * const kPrivateKey = @"Private";
static NSString * const kProfileKey = @"Profile";

@implementation CurrentUser

- (id)initWithDictionary:(NSDictionary *)object
{
    self = [super init];
    if (self) {
        NSLog(@"%@",object);
        self.id = [object valueForKey:@"id"];
        self.username = [object valueForKey:@"username"];
        self.email = [object valueForKey:@"email"];
        self.lastUpdate = [ZEROIFNULL([object valueForKey:@"lastUpdate"]) unsignedIntegerValue];
        self.lastLogin = [ZEROIFNULL([object valueForKey:@"lastLogin"]) unsignedIntegerValue];
        self.location = [[object valueForKey:@"location"] boolValue];
        self.private = [[object valueForKey:@"private"] boolValue];
        self.profile = [object valueForKey:@"profile"];
    }
    return self;
}

- (id)init {
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (id)initWithId:(NSNumber *)newId
                     Email:(NSString*)newEmail
                  Username:(NSString*)newUsername
                   Profile:(NSMutableDictionary*)newProfile
                  Location:(BOOL)newLocation
                   Private:(BOOL)newPrivate
                 LastLogin:(NSUInteger)newLastLogin
                LastUpdate:(NSUInteger)newLastUpdate
{
    self = [super init];
    if (self) {
        self.id = newId;
        self.email = newEmail;
        self.username = newUsername;
        self.profile = newProfile;
        self.location = newLocation;
        self.profile = newProfile;
        self.lastLogin = newLastLogin;
        self.lastUpdate = newLastUpdate;
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.id forKey:kIdKey];
    [encoder encodeObject:self.email forKey:kEmailKey];
    [encoder encodeObject:self.username forKey:kUsernameKey];
    [encoder encodeObject:self.profile forKey:kProfileKey];
    [encoder encodeBool:self.location forKey:kLocationKey];
    [encoder encodeBool:self.private forKey:kPrivateKey];
    [encoder encodeObject:[NSNumber numberWithUnsignedInteger:self.lastLogin] forKey:kLastLoginKey];
    [encoder encodeObject:[NSNumber numberWithUnsignedInteger:self.lastUpdate] forKey:kLastUpdateKey];
}

- (id)initWithCoder:(NSCoder *)decoder
{
    return [self initWithId:[decoder decodeObjectForKey:kIdKey]
                      Email:[decoder decodeObjectForKey:kEmailKey]
                   Username:[decoder decodeObjectForKey:kUsernameKey]
                    Profile:[decoder decodeObjectForKey:kProfileKey]
                   Location:[decoder decodeBoolForKey:kLocationKey]
                    Private:[decoder decodeBoolForKey:kPrivateKey]
                  LastLogin:[ZEROIFNULL([decoder decodeObjectForKey:kLastLoginKey]) unsignedIntegerValue]
                 LastUpdate:[ZEROIFNULL([decoder decodeObjectForKey:kLastUpdateKey]) unsignedIntegerValue]
            ];
}

@end
