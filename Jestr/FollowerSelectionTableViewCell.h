//
//  FollowerSelectionTableViewCell.h
//  Jestr
//
//  Created by Árpád Kiss on 2014.07.11..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User_Public.h"

@interface FollowerSelectionTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (nonatomic, assign) id delegate;

-(void)configureCell:(User_Public*)user;

@end

@protocol FollowerSelectionDelegate

-(void)followerSelected:(User_Public*)user;
-(void)followerDeselected:(User_Public *)user;

@end
