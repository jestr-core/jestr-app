//
//  MenuView.m
//  Jestr
//
//  Created by Árpád Kiss on 2014.07.11..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import "MenuView.h"
#import "Helper.h"

@implementation MenuView {
    BOOL _isMenuHidden;
    NSInteger _menuStep;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = [Helper sharedHelper].secondaryColor;
        [self.menuButtons enumerateObjectsUsingBlock:^(MenuButton* obj, NSUInteger idx, BOOL *stop) {
            switch (idx) {
                case 0: // notif
                    [obj setImage:[UIImage imageNamed:@"menuIconNotif.png"] forState:UIControlStateNormal];
                    break;
                case 1: // profile
                    [obj setImage:[UIImage imageNamed:@"menuIconProfile.png"] forState:UIControlStateNormal];
                    break;
                case 2: // messages
                    [obj setImage:[UIImage imageNamed:@"menuIconMessages.png"] forState:UIControlStateNormal];
                    break;
                case 3: // explore
                    [obj setImage:[UIImage imageNamed:@"menuIconExplore.png"] forState:UIControlStateNormal];
                    break;
                case 4: // search
                    [obj setImage:[UIImage imageNamed:@"menuIconSearch.png"] forState:UIControlStateNormal];
                    break;
                default:
                    break;
            }
        }];
        [self.toggleMenuButton setImage:[UIImage imageNamed:@"menuOpen.png"] forState:UIControlStateNormal];
        [self.toggleMenuButton setImage:[UIImage imageNamed:@"menuClose.png"] forState:UIControlStateSelected];
        
        _menuStep = 50;
        
        _isMenuHidden = YES;

    }
    return self;
}

- (IBAction)toggleMenu:(MenuButton*)sender {
    if(_isMenuHidden) {
        
    }
    _isMenuHidden = !_isMenuHidden;
}

- (IBAction)editorButtonPressed:(id)sender {
    
}

- (IBAction)menuButtonPressed:(id)sender {
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
