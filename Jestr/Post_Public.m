//
//  Post_Public.m
//  Jestr
//
//  Created by Árpád Kiss on 2014.07.22..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import "Post_Public.h"
#import "Comment_Preview.h"
#import "Cool_Preview.h"
#import "User_Public.h"


@implementation Post_Public

@dynamic created_time;
@dynamic hashtags;
@dynamic id;
@dynamic images;
@dynamic type;
@dynamic author;
@dynamic comments;
@dynamic coolerz;

@end
