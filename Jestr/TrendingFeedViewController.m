//
//  TrendingFeedViewController.m
//  Jestr
//
//  Created by Árpád Kiss on 2014.07.03..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import "TrendingFeedViewController.h"
#import "Post_Public.h"
#import "CommentViewController.h"
#import "CoolersViewController.h"
#import "APIAgent.h"
#import "Comment_Public.h"
#import "Comment_Preview.h"
#import "User_Public.h"
#import "Helper.h"
#import "ProfileFeedViewController.h"

@interface TrendingFeedViewController () {
    Post_Public* _tmpPost;
    NSIndexPath* _tmpIndexPath;
    NSArray* _posts;
    NSManagedObjectContext* _tmpContext;
    UIRefreshControl* _r;
    int _page;
    NSPredicate* _trendingPredicate;
}

@end

@implementation TrendingFeedViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _page = 0;
    
    _trendingPredicate = [NSPredicate predicateWithFormat:@"type = 2"];
    
    _tmpContext = [NSManagedObjectContext MR_contextWithParent:[NSManagedObjectContext MR_contextForCurrentThread]];

    UINib* nib = [UINib nibWithNibName:@"PostPublicTableViewCell" bundle:nil];
    
    [self.trendingTableView registerNib:nib forCellReuseIdentifier:kFeedCellReuseIdentifier];

    _r = [[UIRefreshControl alloc] init];
    _r.tintColor = [Helper sharedHelper].secondaryColor;
    [_r addTarget:self action:@selector(refreshTrending) forControlEvents:UIControlEventValueChanged];
    [self.trendingTableView addSubview:_r];
    
    //[self fetchPosts];
    [self loadTrending];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self setUpRecognizer];
    [self checkTempPost];
}

#pragma mark Tableview gesture recognizer

-(void)setUpRecognizer {
    UISwipeGestureRecognizer *r = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(didSwipe:)];
    [r setDirection:UISwipeGestureRecognizerDirectionRight];
    [self.trendingTableView addGestureRecognizer:r];
    r = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(didSwipe:)];
    [r setDirection:UISwipeGestureRecognizerDirectionLeft];
    [self.trendingTableView addGestureRecognizer:r];
}

- (void)didSwipe:(UIGestureRecognizer *)gestureRecognizer {
    CGPoint swipeLocation = [gestureRecognizer locationInView:self.trendingTableView];
    NSIndexPath *swipedIndexPath = [self.trendingTableView indexPathForRowAtPoint:swipeLocation];
    PostPublicTableViewCell* swipedCell = (PostPublicTableViewCell*)[self.trendingTableView cellForRowAtIndexPath:swipedIndexPath];
    [swipedCell performSwipe:gestureRecognizer];
    NSLog(@"triggered swipe on %@", swipedCell);
    NSLog(@"%@", gestureRecognizer);
}

#pragma mark data

-(void)refreshTrending {
    _page = 0;
    [self loadTrending];
}

//- (void) fetchPosts {
//    _posts = [Post_Public MR_findAllSortedBy:@"created_time" ascending:NO];
//    
//    if(_posts.count == 0) {
//        [self loadTrending];
//    }
//}

-(void)checkTempPost {
    if(_tmpPost != nil) {
        NSString* postEndpoint = [@"posts/" stringByAppendingString:_tmpPost.id.description];
        [[APIAgent sharedAgent] getFromEndpoint:postEndpoint onSuccess:^(id responseObject) {
            NSDictionary* r = (NSDictionary*)responseObject;
            NSDictionary* cl = [r objectForKey:@"comments"];
            
            Comment_Preview* cp = [Comment_Preview MR_createInContext:_tmpContext];
            
            NSArray* comments = [cl objectForKey:@"data"];
            
            cp.count = [cl objectForKey:@"count"];
            cp.data = [NSSet new];
            
            NSMutableSet* tSet = [[NSMutableSet alloc] init];
            
            if(![comments isEqual:[NSNull null]]) {

            
            [comments enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                NSDictionary* current = (NSDictionary*)obj;
                NSDictionary* from = [current objectForKey:@"from"];
                
                Comment_Public* cl = [Comment_Public MR_createInContext:_tmpContext];
                cl.id = [current objectForKey:@"id"];
                cl.message = [current valueForKey:@"message"];
                
                cl.created_time = [current objectForKey:@"created_time"];
                
                User_Public *p = [User_Public MR_createInContext:_tmpContext];
                p.id = [from objectForKey:@"id"];
                p.username = [from valueForKey:@"username"];
                p.profile_picture = NILIFNULL([from objectForKey:@"profile_picture"]);
                
                cl.from = p;
                
                [tSet addObject:cl];
            }];
            
            cp.data = [NSSet setWithSet:tSet];
                
            }
            
            NSDictionary* coolz = [responseObject objectForKey:@"coolerz"];
            
            Cool_Preview* cool = [Cool_Preview MR_createInContext:_tmpContext];
            
            cool.count = [coolz objectForKey:@"count"];
            
            NSArray* coolerz = [coolz objectForKey:@"data"];
            
            tSet = [[NSMutableSet alloc] init];
            
            if(![coolerz isEqual:[NSNull null]]) {
                
                
                [coolerz enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    [tSet addObject:[User_Public MR_importFromObject:obj inContext:_tmpContext]];
                }];
                
                cool.data = [NSSet setWithSet:tSet];
            }
            
            _tmpPost.comments = cp;
            _tmpPost.coolerz = cool;
            
            [self.trendingTableView reloadRowsAtIndexPaths:@[_tmpIndexPath] withRowAnimation:UITableViewRowAnimationNone];
            
        } onFail:^(id error) {
            
        }];
    }
}

#pragma mark - TableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _posts.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PostPublicTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kFeedCellReuseIdentifier];
    
    [cell forceResetCell];
    
    NSInteger idx = indexPath.row;
    
    Post_Public *row = _posts[idx];
    
    cell.post = row;
    
    ProfileActionType type = ([row.author.id isEqual:[Helper sharedHelper].currentUser.id]) ? own : friend;
    
    [cell configureCellforType:type withIndexPath:indexPath];
    
    cell.delegate = self;
    
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 320;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)loadTrending{
    NSString* feedUrl = [@"posts/trending?page=" stringByAppendingFormat:@"%i", _page];
    [[APIAgent sharedAgent] getFromEndpoint:feedUrl onSuccess:^(id responseObject) {
        NSLog(@"DATA: %@", responseObject);
        
        if(![responseObject isEqual:[NSNull null]]) {
            
            __block NSPredicate* p;
            __block NSArray* pos;
            [responseObject enumerateObjectsUsingBlock:^(NSDictionary* obj, NSUInteger idx, BOOL *stop) {
                p = [NSPredicate predicateWithFormat:@"id = %d", [obj valueForKey:@"id"]];
                pos = [Post_Public MR_findAllWithPredicate:p inContext:_tmpContext];
                Post_Public* post;
                if(pos.count == 0) {
                    post = [Post_Public MR_importFromObject:obj inContext:_tmpContext];
                } else {
                    post = (Post_Public*)pos[0];
                }
                post.type = @2;
            }];
            
            _posts = [Post_Public MR_findAllSortedBy:@"created_time" ascending:NO withPredicate:_trendingPredicate inContext:_tmpContext];
            
            [self.trendingTableView reloadData];
            [_r endRefreshing];
        } else {
            _page = (_page == 0) ? 0 : _page - 1;
        }
        
    } onFail:^(NSError *error) {
        NSLog(@"%@",error);
    }];
    
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row == _posts.count - 1) {
        _page = _page + 1;
        [self loadTrending];
    }
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    if([segue.identifier isEqual:@"ToCommentFromExplore"]) {
        CommentViewController* c = (CommentViewController*)segue.destinationViewController;
        c.post = _tmpPost;
    } else if([segue.identifier isEqual:@"ToProfileFeedViewFromExplore"]) {
        ProfileFeedViewController* c = (ProfileFeedViewController*)segue.destinationViewController;
        if([sender isKindOfClass:[Comment_Public class]]) {
            Comment_Public* co = (Comment_Public*)sender;
            c.public = co.from;
        } else {
            c.public = _tmpPost.author;
        }
    } else if([segue.identifier isEqual:@"ToCoolersFromExplore"]) {
        CoolersViewController* c = (CoolersViewController*)segue.destinationViewController;
        c.preview = _tmpPost.coolerz;
        c.postId = _tmpPost.id;
    }
}

#pragma mark delegate methods

-(void)onCommentButtonPress:(Post_Public*)post
                 withSender:(PostPublicTableViewCell*)cell
               andIndexPath:(NSIndexPath*)path
{
    _tmpIndexPath = path;
    _tmpPost = post;
    [cell resetCell];
    [self performSegueWithIdentifier:@"ToCommentFromExplore" sender:nil];
}

-(void)onProfileAction:(Post_Public*)post
        withActionType:(ProfileActionType)type
             andSender:(PostPublicTableViewCell*)cell
          andIndexPath:(NSIndexPath*)path
{
    switch (type) {
        case friend: {
            // unfollow
            NSString* followEndpoint = [[@"users/" stringByAppendingString:post.author.id.description] stringByAppendingString:@"/followers"];
            
            [[APIAgent sharedAgent] deleteFromEndpoint: followEndpoint withParams:@{} onSuccess:^(id responseObject) {
                //code
                [self refreshTrending];
            } onFail:^(id error) {
                //code
            }];
            break;
        }
        case own: {
            //deletePost
            _tmpPost = post;
            _tmpIndexPath = path;
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle:@"Confirm!"
                                  message:@"Are you sure you want to delete this post?"
                                  delegate: self
                                  cancelButtonTitle:@"Cancel"
                                  otherButtonTitles:@"OK", nil];
            [alert show];
            break;
        }
        case unknown: {
            // follow
            NSString* followEndpoint = [[@"users/" stringByAppendingString:post.author.id.description] stringByAppendingString:@"/followers"];
            
            [[APIAgent sharedAgent] postToEndpoint: followEndpoint withParams:@{} onSuccess:^(id responseObject) {
                //code
            } onFail:^(id error) {
                //code
            }];
            break;
        }
        default:
            break;
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if(buttonIndex == 1)
    {
        NSString* deletePostEndpoint = [@"posts/" stringByAppendingString:_tmpPost.id.description];
        [[APIAgent sharedAgent] deleteFromEndpoint: deletePostEndpoint withParams:@{} onSuccess:^(id responseObject) {
            //code
            [_tmpPost MR_deleteEntity];
            _posts = [Post_Public MR_findAllSortedBy:@"created_time" ascending:NO withPredicate:_trendingPredicate inContext:_tmpContext];
            [self.trendingTableView reloadData];
        } onFail:^(id error) {
            //code
        }];
    }
    
}


-(void)onCommentsLabelTouched:(Post_Public*)post
                   withSender:(PostPublicTableViewCell*)cell
                 andIndexPath:(NSIndexPath*)path
{
    NSLog(@"comments label tapped.");
    _tmpIndexPath = path;
    _tmpPost = post;
    [cell resetCell];
    [self performSegueWithIdentifier:@"ToCommentFromExplore" sender:nil];
    
}

-(void)onCoolersLabelTouched:(Post_Public*)post
                  withSender:(PostPublicTableViewCell*)cell
                andIndexPath:(NSIndexPath*)path
{
    NSLog(@"coolers label tapped.");
    _tmpIndexPath = path;
    _tmpPost = post;
    [cell resetCell];
    [self performSegueWithIdentifier:@"ToCoolersFromExplore" sender:nil];
}

-(void)onUsernameLabelTouched:(Post_Public*)post
                   withSender:(PostPublicTableViewCell*)cell
                 andIndexPath:(NSIndexPath*)path {
    NSLog(@"username label tapped.");
    _tmpIndexPath = path;
    _tmpPost = post;
    [cell resetCell];
    [self performSegueWithIdentifier:@"ToProfileFeedViewFromExplore" sender:nil];
    
}

-(void)onDoubleTap:(Post_Public *)post withSender:(PostPublicTableViewCell *)cell andIndexPath:(NSIndexPath *)path {
    
    _tmpIndexPath = path;
    _tmpPost = post;
    
    NSString* postEndpoint = [[@"posts/" stringByAppendingString:post.id.description] stringByAppendingString:@"/coolers"];
    
    [[APIAgent sharedAgent] postToEndpoint:postEndpoint withParams:@{} onSuccess:^(id responseObject) {
        [self checkTempPost];
    } onFail:^(id error) {
        NSLog(@"%@", error);
    }];
    
}

-(void)onCommentUsernameLabelTouched:(Comment_Public *)comment withPost:(Post_Public *)post withSender:(PostPublicTableViewCell *)cell andIndexPath:(NSIndexPath *)path {
    _tmpIndexPath = path;
    _tmpPost = post;
    [self performSegueWithIdentifier:@"ToProfileFeedView" sender:comment];
}

@end
