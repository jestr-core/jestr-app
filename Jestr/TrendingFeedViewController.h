//
//  TrendingFeedViewController.h
//  Jestr
//
//  Created by Árpád Kiss on 2014.07.03..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PostPublicTableViewCell.h"

@interface TrendingFeedViewController : UIViewController <
UITableViewDelegate,
UITableViewDataSource,
PostPublicTableViewCellDelegate
>


@property (weak, nonatomic) IBOutlet UITableView *trendingTableView;

@end
