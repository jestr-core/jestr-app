//
//  NotifUserInteractionTableViewCell.m
//  Jestr
//
//  Created by Árpád Kiss on 2014.07.03..
//  Copyright (c) 2014 Peabo Media LLC. All rights reserved.
//

#import "NotifUserInteractionTableViewCell.h"
#import "User_Public.h"
#import "UIImageView+AFNetworking.h"

@implementation NotifUserInteractionTableViewCell {
    NSString* _message;
    Notification* _notif;
}

- (void)awakeFromNib
{
    // Initialization code
}

-(void)configureCell:(Notification*)notif withMessage:(NSString*)message {
    _notif = notif;
    NSString *profileImageURL = [@"http://api.jestr.me/"
                                 stringByAppendingString:EMPTYIFNIL(                                                       [notif.from.profile_picture valueForKey:@"medium"])];
    [self.profileImageView setImageWithURL:[NSURL URLWithString:profileImageURL]];
    
    _message = message;
    self.messageLabel.text = _message;
    if(!notif.confirmed || ![notif.type isEqualToString:@"follow_request_confirmation"]) {
        [self.actionButton addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
    } else {
        self.actionButton.hidden = YES;
    }
}

-(void)buttonClick:(id)sender {
    if([self.delegate respondsToSelector:@selector(onButtonClick:)]) {
        [self.delegate onButtonClick:_notif];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)drawRect:(CGRect)rect {
    self.profileImageView.layer.cornerRadius = self.profileImageView.bounds.size.width / 2;
    self.profileImageView.layer.masksToBounds = YES;
    self.profileImageView.layer.borderWidth = 1;
    self.profileImageView.layer.borderColor = [[UIColor whiteColor] CGColor];
    
    self.messageLabel.numberOfLines = 0;
    self.messageLabel.lineBreakMode = NSLineBreakByWordWrapping;
    
    CGRect r = [_message boundingRectWithSize:CGSizeMake(236, 0)
                                        options:NSStringDrawingUsesLineFragmentOrigin
                                     attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15]}
                                        context:nil];
    
    self.messageLabel.frame = CGRectMake(self.messageLabel.frame.origin.x, self.messageLabel.frame.origin.y, self.messageLabel.frame.size.width, r.size.height);

}

@end
